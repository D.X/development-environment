# OurPlant OS - Design Rules

This repository contains the design rules for the OurPlant OS. These rules have to be regarded by every developer working on the OurPlant OS System. The compliance with the rules will be checked at each commit to the OurPlant OS.

## How to use the design rules

The design rules are subdivided into single articles. The articles are organized and maintained within this repository. This readme is the start point of the design rules and holds the table of content. Each item is linked with this readme.

## Who is maintaining the design rules

The design rules are maintained from a group of official certified OurPlant OS developers. This group is also responsible for checking the compliance with the design rules at an OurPlant OS commit. Everybody is invited to use the ticket system of this repository to propose changes in the design rules.

## What is the format of the design rules

The design rules must be written in markdown. Here standard markdown with [GitLab flavour](https://docs.gitlab.com/ee/user/markdown.html) has to be used. Each Article has to be committed. A direct manipulation within the repository is not allowed. Other formats than markdown are not allowed for articles.

# Content

The design rules consists of the following articles:

[Style Guide Summary](articles/style_guide_summary.md)

[Language](/articles/language.md)
[File Head](/articles/file_head.md)
[Names, Namespace, Units](/articles/names.md)
[Naming Convention](articles/naming_convention.md)
[Readability](articles/readability.md)
[Statements](articles/statements.md)
[Style Tips](articles/style_tips.md)
[File-Formats](articles/FileFormats.md)

old versionen:
[StyleGuide - V9](/old_stuff/StyleGuide.md)


---------------------------------------
Copyright &copy; 2021 Häcker Automation 