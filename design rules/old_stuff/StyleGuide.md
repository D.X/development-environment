# Styleguide & Dokumentation V.9

# Vorlagen
http://www.delphi-treff.de/object-pascal/styleguide/

http://edn.embarcadero.com/article/10280

# Historie

| Änderungen                                     | Datum      | Autor              |
| :--------------------------------------------- | :--------- | :----------------- |
| Dokument erstellt                              | 11.07.2014 | Ulf Böttner-Common |
| um Clean Code Regeln erweitert                 | 28.07.2014 | Ulf Böttner-Common |
| Beschluss im Teammeeting über Styleguide       | 30.07.2014 | Ulf Böttner-Common |
| Überarbeitung Vorlagen                         | 31.07.2014 | Ulf Böttner-Common |
| Empfehlung mit strict zu arbeiten              | 13.08.2014 | Ulf Böttner-Common |
| TTimer Präfix                                  | 21.08.2014 | Ulf Böttner-Common |
| Korrektur Label Beispiel                       | 29.08.2014 | Ulf Böttner-Common |
| enums, records, commits auf English            | 25.09.2014 | Ulf Böttner-Common |
| TPopupMenu                                     | 24.10.2014 | Ulf Böttner-Common |
| Namespace: Groß- Kleinschreibung und Test      | 07.11.2014 | Ulf Böttner-Common |
| Korrektur uses                                 | 04.04.2018 | Ulf Böttner-Common |
| Geänderte Präfixe; Formatierung und Aufbau neu | 18.05.2018 | Karl Kurt Blockus  |

# Inhalt

- [Styleguide & Dokumentation V.9](#styleguide--dokumentation-v9)
- [Vorlagen](#vorlagen)
- [Historie](#historie)
- [Inhalt](#inhalt)
- [Literatur](#literatur)
- [Sprache](#sprache)
- [Dateikopf](#dateikopf)
- [Namen, Namespace und Uses](#namen-namespace-und-uses)
  - [Units](#units)
  - [Schreibweise](#schreibweise)
  - [Uses-Reihenfolge](#uses-reihenfolge)
  - [interface und implementation](#interface-und-implementation)
  - [initialization und finalization](#initialization-und-finalization)
  - [Globale Variablen](#globale-variablen)
  - [Globale Konstanten](#globale-konstanten)
- [Namenskonventionen](#namenskonventionen)
  - [Typen wie records, enums, etc](#typen-wie-records-enums-etc)
  - [Klassen-Benennung](#klassen-benennung)
    - [Sichtbarkeit](#sichtbarkeit)
    - [Benennung von Attributen](#benennung-von-attributen)
    - [Methoden-Benennung](#methoden-benennung)
    - [Konstruktoren, Destruktoren, etc.](#konstruktoren-destruktoren-etc)
  - [Setter und Getter](#setter-und-getter)
  - [Boolean](#boolean)
  - [Properties](#properties)
  - [Interfaces](#interfaces)
    - [Interface-Deklarationen](#interface-deklarationen)
    - [Anordnung des Interface-Hauptteils](#anordnung-des-interface-hauptteils)
  - [Exceptions](#exceptions)
  - [Lokale Konstanten](#lokale-konstanten)
  - [Lokale Variablen](#lokale-variablen)
  - [Präfixe von GUI-Elementen](#präfixe-von-gui-elementen)
- [Tipps](#tipps)
  - [Magic Numbers](#magic-numbers)
  - [To-Do-Listen verwenden](#to-do-listen-verwenden)
    - [Erstellung einer To-Do-Liste / Hinzufügen von Einträgen](#erstellung-einer-to-do-liste--hinzufügen-von-einträgen)
    - [Einfügen eines To-Do-Listeneintrags als Kommentar im Quelltext](#einfügen-eines-to-do-listeneintrags-als-kommentar-im-quelltext)
    - [Markieren von To-Do-Listeneinträgen als „erledigt“](#markieren-von-to-do-listeneinträgen-als-erledigt)
    - [Filtern von Einträge einer To-Do-Liste](#filtern-von-einträge-einer-to-do-liste)
    - [Löschen eines Eintrages aus der To-Do-Liste](#löschen-eines-eintrages-aus-der-to-do-liste)
  - [Regionen](#regionen)
- [Erhöhung der Lesbarkeit](#erhöhung-der-lesbarkeit)
  - [Automatische Formatierung](#automatische-formatierung)
  - [Verwendung des Leerzeichens](#verwendung-des-leerzeichens)
  - [Leerzeilen](#leerzeilen)
- [Anweisungen](#anweisungen)
  - [Einfache Anweisungen](#einfache-anweisungen)
  - [Zusammengesetzte Anweisungen](#zusammengesetzte-anweisungen)
  - [Zuweisungen und Ausdrucksanweisungen](#zuweisungen-und-ausdrucksanweisungen)
  - [Deklaration lokaler Variablen](#deklaration-lokaler-variablen)
  - [Array-Deklarationen](#array-deklarationen)
  - [Verzweigungen und Schleifen](#verzweigungen-und-schleifen)
  - [Rücksprung aus Funktionen / Funktionen in Übergabeparametern.](#rücksprung-aus-funktionen--funktionen-in-übergabeparametern)
- [Zusammenfassung der Regeln](#zusammenfassung-der-regeln)
  - [TODO](#todo)
    - [Logging](#logging)
- [Dokumentation](#dokumentation)


# Literatur

http://www.clean-code-developer.de/

http://nettundfroh.de/index.php/Clean_Code_Zusammenfassung
 

# Sprache

Englisch wird grundsätzlich verwendet.

- Dokumentation

- Commits und Relase_*.inc Dateien

- Bemerkungen im Code

- Dateinamen

- Verzeichnisse

- Namen von Variablen und Funktionen

Bei Bezeichnern die nicht zum umgangssprachlichen Englisch gehören, kann der deutsche Bezeichner z.B. in die Doku geschrieben werden. Bsp.:

```delphi
// ...

  /// <remarks>

  /// 3D Bestückkopf

  /// </remarks>

// ...

function TForm1.GetPlacerName: string;

begin

  Result := fPlacerName;

end.
```

 

# Dateikopf

Im Dateikopf sollte immer eine Beschreibung stehen:
```delphi
{ ***************************************************************************
           VicoBase

           Copyright (C) 2014 Häcker Automation GmbH

           http://www.haecker-automation.de/

           contact@haecker-automation.com

           Description
*************************************************************************** }
```

Diese Vorlage steht im SVN

>svn://192.168.0.142/svn/VicoBase.V15/configuration/Vorlagen/Delphi

und muss kopiert werden nach:

>C:\Program Files (x86)\Embarcadero\RAD Studio\12.0\ObjRepos\de\Code_Templates\Delphi

Unter Ansicht -> Vorlagen und Doppelklick auf Filehead wird der Header eingefügt.

Anschließend steht der Cursor auf Description. Diese ist durch einen kurzen englischen Text zu ersetzen, z.B. „Base module for placer“.

# Namen, Namespace und Uses
RAD Studio unterstützt jetzt zusätzlich zum Namespace- oder Unit-Namen einen Unit-Gültigkeitsbereichsnamen bzw. ein Präfix. Damit ein Name als voll qualifiziert gilt, muss er den Unit-Gültigkeitsbereichsnamen enthalten.

## Units
Units werden mit einem Namespace versehen, z.B.:

```delphi
unit module.MyModuleA
```

Damit heißt die Datei auch 

>module.MyModuleA.pas

Units werden vollqualifiziert mit `uses` importiert:

```delphi
uses module.MyModuleA
```

## Schreibweise

Der Namespace wird klein geschrieben, das Modul nach der bekannten InfixCaps Schreibweise.

>common.LinkManager.pas

und für den Unittest

>test\common.test.LinkManager.pas

## Uses-Reihenfolge
Vorab sind alle unnötigen `uses`-Anweisungen zu entfernen: 
>Tools &#9658; CnPack &#9658; benutze Cleaner

Weiterhin soweit es möglich ist, die uses in den implementation Teil verschieben.

Alles uses-Anweisungen sind vollqualifiziert anzugeben in folgender Reihenfolge:

1. Units im selben Projekt
2. Units aus der Häckersoftware wie z.B. Module, Bibliotheken etc.
3. Drittanbieter, z.B. Jedi Komponenten
4. Vollqualifizierte Systemkomponenten

Pro Zeile eine uses Anweisung!

```delphi
unit Base.PlugInDemo

interface

uses

  Base.PlugInBase,  // Part of the same project.
  Lib.FolderMon,    // Unit from a module or library.
  JvPlugIn,         // Third party tools.
  JvJVCLUtils,
  JvComponentBase,
  System.SysUtils,  // Fully qualified system components.
  System.Classes,
  Vcl.Forms;
  // ...

implementation
```

## interface und implementation

Der interface-Abschnitt dient dazu, Funktionen, Prozeduren, Typen usw. dieser Unit anderen Units zur Verfügung zu stellen. Alles, was hier steht, kann von außen verwendet werden. Bei Prozeduren und Funktionen steht hier nur der Kopf der Routine (Name, Parameter und evtl. Rückgabewert). Die Definition folgt dann im Abschnitt `implementation`. Der `interface`-Abschnitt endet mit Beginn des `implementation`-Abschnitts.

## initialization und finalization
Bei Bedarf können am Ende einer Unit noch zwei Abschnitte stehen: `initialization` und `finalization`.

`initialization` muss dabei als erstes aufgeführt werden. Hier werden alle Befehle aufgeführt, die bei Programmstart der Reihe nach ausgeführt werden sollen. Danach folgt das Ende der Unit (end.) oder der `finalization`-Abschnitt. Dieser ist das Gegenstück zu `initialization`. Hier können z. B. vor Programmende Objekte freigegeben werden.

`finalization` kann nur verwendet werden, wenn es auch einen `initialization`-Abschnitt gibt; `initialization` kann jedoch auch ohne `finalization` vorkommen. Eine Unit funktioniert allerdings auch ohne diese Abschnitte.

```delphi
unit Unit1;

interface

implementation

initialization

finalization

end.
```

## Globale Variablen
Die Verwendung von globalen Variablen sollte auf jeden Fall vermieden werden.

Dies gilt auch für die Variablen, die automatisch beim Erstellen einer Form angelegt werden.

```delphi
// ...

var // Comment this out or delete it altogether, unless it’s StartForm!

  Form1: TForm1; // Same here!

// ...
```

Danach erscheint ein Fehler in der `dpr;  Application.CreateForm(TForm1, Form1);` ebenfalls löschen

## Globale Konstanten

Globale Konstanten sollten in einer zentralen Datei „gesammelt“ werden und werden in Großbuchstaben geschrieben. Wörter werden mit Unterstrichen getrennt. Konstanten beginnen immer mit dem Präfix C_

```delphi
const C_MYMODULE_START = 1;
```

# Namenskonventionen

Außer bei reservierten Wörtern und Direktiven, die in Kleinbuchstaben sind, sollten alle Pascal-Bezeichner Camel Caps verwenden, was bedeutet, dass der erste Buchstabe ein Großbuchstabe und jedes in einem Bezeichner enthaltene Wort ebenso wie eingebettete Abkürzungen in Großbuchstaben sein sollten.

`MyIdentifier`
`MyFTPClass`

## Typen wie records, enums, etc

… beginnen mit einem großen T.
```delphi
type

  TColor = (Black, Yellow, Red);

  // Enumeration values, Mon = 1, Tue = 2, ..., Sun = 7.

  TDay = (Mon = 1, Tue, Wed, Thu, Fri, Sat, Sun);

  // Enumeration values, Hearts = 13, Diamonds = 14, Clubs = 22, Spades = 23.

  TSuit = (Hearts=13, Diamonds, Clubs = 22, Spades);

  TEmployee = record

    Name: string;

end;
```

## Klassen-Benennung
Klassennamen bekommen als Präfix ein T

`TFolderMon = class;`

### Sichtbarkeit
| <div style="width:150px">Name</div> | Beschreibung                                                                                                                                                                                     |
| :---------------------------------- | :----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| `strict private`                    | Elemente sind nur innerhalb der Klasse sichtbar. Andere Klassen können nicht darauf zugreifen, auch wenn sie sich in derselben Unit befinden.                                                    |
| `strict protected`                  | Elemente sind innerhalb der Klasse und in allen davon abgeleiteten Klassen sichtbar, jedoch nicht in anderen (nicht verwandten) Klassen derselben                                                |
| `private`                           | Elemente können nur innerhalb der gleichen Unit verwendet werden. Aus anderen Units ist ein Zugriff nicht möglich.                                                                               |
| `protected`                         | Elemente sind wie private-Elemente innerhalb der gleichen Unit verwendbar. Darüberhinaus haben alle abgeleiteten Klassen darauf Zugriff, unabhängig davon, in welcher Unit sie sich befinden.    |
| `public`                            | Elemente unterliegen keinen Zugriffsbeschränkungen.                                                                                                                                              |
| `published`                         | Elemente haben dieselbe Sichtbarkeit wie public-Elemente. Zusätzlich werden diese Element im Objektinspektor angezeigt, weshalb nicht alle Typen als published-Element eingesetzt werden können. |

Es wird empfohlen mit strict zu arbeiten!

```delphi
type

  TfrmPlugInDemo = class(TfrmPlugInBase)
  strict private
  { Strict Private declarations }

  strict protected
  { Strict Protected declarations }

  private
  { Private-Deklarationen }

  protected
  { Protected -Deklarationen }

  public
  { Public-Deklarationen }

  published
  { Published-Deklarationen }

end;
```

### Benennung von Attributen
Verwendung von Camel Caps. Jede Typdeklaration beginnt mit einem kleinen f

Deklariere alle Datentypen im private-Abschnitt und verwende Properties oder Get- und Set-Methoden für den Zugriff darauf.

**Correct:**
```delphi
fMyString: string;
```

**Incorrect:**
```delphi
lpstrMyString: string;
```

### Methoden-Benennung
Für Methodennamen ist ebenfalls der Camel Caps-Stil zu verwenden. Das erste Wort beginnt bei `public`-Methoden mit einem Großbuchstaben, sonst mit einem Kleinbuchstaben. Jedes weitere Wort beginnt mit einem Großbuchstaben, Abkürzungen werden komplett groß geschrieben. Alle weiteren Buchstaben sind kleine. Namen sollten kurze, bezeichnende Sätze sein.

**GOOD method naming:**
```delphi
ShowStatus, DrawCircle, AddLayoutComponent
```
 

**BAD method naming:**
```delphi
MouseButton           // Not a sentence, does not describe what the method does.

add_layout_component  // Underscores are not allowed.

ServerRunning         // What does this do? Does it check if the server is running

                      // (better name would be: IsServerRunning)?

                      // Does it start the server (better name: StartServer)?
```

### Konstruktoren, Destruktoren, etc.

Beim Instanziieren einer Klasse werden verschiedene Prozeduren durchlaufen. Diese Stellen sollten verwendet werden um andere Klassen, Variablen etc. zu instanziieren, initialisieren, zerstören, usw.

Loaded &#9658; Create &#9658; AfterConstruction &#9658; … &#9658; BeforeDestruction &#9658; Destroy

```delphi
protected

  procedure Loaded; override;

public

  constructor Create(aOwner: TComponent); override;

  procedure AfterConstruction; override;

  procedure BeforeDestruction; override;

  destructor Destroy; override;
```

## Setter und Getter

Der Zugriff auf Klassenattribute sollte nie unmittelbar geschehen, sondern durch Get- und Set-Methoden.

```delphi
private

  var

    fHeight: Real;

public

  procedure SetHeight(aNewHeight: Integer);

  function GetHeight: Integer;
```

## Boolean

Um boolesche Werte abzufragen, werden Is-Funktionen verwendet.

```delphi
private

  var

    fResizable: Boolean;

public

  function IsResizable: Boolean;
```

## Properties

… haben keinen Präfix.

```delphi
type

  TCar = class

  private

    fColor: TColor;

    procedure SetMyColor (const Color: TColor);

  public

    property Color: TColor read fColor write SetMyColor;

  end;
```

## Interfaces

Interfaces bekommen den Präfix I. Ansonsten folgen sie den Regeln für Klassendefinitionen.

```delphi
type

  IMath = interface(IInterface)

  function Add(aSum1, aSum2: Integer): Integer;

  function Sub(aSub1, aSub2: Integer): Integer;

end
```

### Interface-Deklarationen

Interfaces werden entsprechend den Klassendeklarationen deklariert:

```delphi
IMyInterface = interface

  // Interface body.

end;
```

In einer Interface-Deklaration gibt es keine Felder, Properties dagegen sind erlaubt. Alle Interface-Methoden sind public und abstract; verwenden Sie diese Schlüsselwörter nicht extra in der Deklaration einer Interface-Methode.

### Anordnung des Interface-Hauptteils

Der Hauptteil einer Interface-Deklaration sollte in folgender Reihenfolge geordnet sein:

1. Interface-Methodendeklarationen
2. Interface-Eigenschaftendeklarationen

Die Deklarationsstile von Interfaceeigenschaften und -methoden sind mit den Stilen von Klasseneigenschaften und -methoden identisch.

## Exceptions

Exceptions haben den Präfix `E`

```delphi
EMathError = class(Exception)
```

## Lokale Konstanten

Lokale Konstanten haben den Präfix `C_` und bestehen komplett aus Großbuchstaben bzw. Unterstrichen zur Worttrennung. Sie sollten anstatt von „Magic Numbers“ eingesetzt werden.

## Lokale Variablen

Lokale Variablen haben den Präfix `v`, Übergabeparameter `a` davorgesetzt und sind `const`.

**Ausnahmen:** Variablen für Schleifen.

```delphi
procedure TMyClass.SetName(const aName, aFirstName: string);

  var
    vSurName  : string;
    I         : Integer;

  const
    C_MAX_LOOP_INDEX = 9;

  begin
    fFirstName  := aName;
    SurName     := ‘Müller’;

    for I := 0 to C_MAX_LOOP_INDEX do
    // ...
```

 

## Präfixe von GUI-Elementen

| Typ          | Präfix        | Beispiel       |
| :----------- | :------------ | :------------- |
| TForm        | (kein)        | Form1          |
| TFrame       | frm           | frmFrame1      |
| TMainMenu    | mm            | mmMenu1        |
| TLabel       | l (kleines L) | lMyLabel       |
| TEdit        | e             | eName          |
| TMemo        | mmo           | mmoTextField   |
| TButton      | b             | bOK            |
| TCheckBox    | chk           | chkCheckbox1   |
| TRadioButton | rb            | rbRadioButton1 |
| TListBox     | lst           | lstList1       |
| TComboBox    | cb            | cbComboBox1    |
| TScrollBar   | sb            | sbVertical     |
| TGroupBox    | gb            | gbGroupBox1    |
| TRadioGroup  | rg            | rgRadioGroup1  |
| TPanel       | p             | pPanel1        |
| TBitBtn      | bi            | biBitButton1   |
| TToolBar     | tb            | tbToolBar1     |
| TTimer       | tmr           | tmrTimer1      |
| TPopupMenu   | pm            | pmPopup1       |
 

# Tipps

Checke den Code etwas sauberer ein als du ihn ausgecheckt hast. Immer funktionelle Änderungen und Code-Aufräumarbeiten getrennt einchecken, sonst wird es unübersichtlich!

Gesetz von LeBlanc: Was du später machen willst, wirst du nie machen.

Besser nicht dokumentieren, als schlecht dokumentieren. Der Code sollte selbsterklärend sein.

Aussagekräftige Namen verwenden!

**Hinweis:** Die meisten nachfolgenden Beispiele entsprechen sehr oft nicht unseren Richtlinien. Es sollen hier lediglich bestimmte „schlechte“ Programmierstile verdeutlicht werden und wie man diese am besten vermeidet.

**Bad variable naming:**

```delphi
int etd;

int dsc;

int dsm;

int fad;
```
 

**Good variable naming:**

```delphi
int ElapsedTimeInDays;

int DaysSinceCreation;

int DaysSinceModification;

int FileAgeInDays;
```

 

**<span style="color:red">BAD STYLE:</span>**
```delphi
public List<int[]> getThem() {
       List<int[]> list1 = new ArrayList<int[]>();

for (int[] x : theList) {
              if (x[0] == 4) {
                    list1.add(x);
              }
}
       return list1;
}
```
 

**<span style="color:green">GOOD STYLE:</span>**
```delphi
public List<int[]> getFlaggedCells() {
       List<int[]> flaggedCells = new ArrayList<inst[]>();

       for (int[] cell : gameBoard) {
              if (cell[STATUS_VALUE] == FLAGGED) {
                    flaggedCells.add(cell);
              }
       }
       return flaggedCells;
}
```
 

**<span style="color:red">BAD STYLE:</span>**
```delphi
Class DtaRcrd102 {
       Private Date genymdhms;
       Private Date modymdhms;
       private final String pszqint = „102“;
       /* ... */
}
```
 

**<span style="color:green">GOOD STYLE:</span>**
```delphi
Class Customer {
       Private Date generationTimestamp;
       Private Date modificationTimestamp;
       Private final String recordId = „102“;
       /* ... */
}
```

## Magic Numbers

Anstelle von aussagekraftlosen Zahlen sollten lokale Konstanten verwendet werden. Ausnahme: Zugriff auf Listenelement [0] o.ä.

**<span style="color:red">BAD STYLE:</span>**

```delphi
if data.length < 1048576 then
begin
  // ...
end;

if status = 2 then
begin
  // ...
end;
```
 
**<span style="color:green">GOOD STYLE:</span>**
```delphi
if datalength < C_MAX_FILE_SIZE_KILOBYTES then
begin
  // ...
end;

if status = C_STATUS_ACTIVE then
begin
  // ...
end;
```

## To-Do-Listen verwenden

http://docwiki.embarcadero.com/RADStudio/XE3/de/To-Do-Listen_verwenden

In einer To-Do-Liste werden die Programmierarbeiten eingetragen und angezeigt, die in einem Projekt noch ausgeführt werden müssen.

### Erstellung einer To-Do-Liste / Hinzufügen von Einträgen
Wählen Sie Ansicht &#9658; To-Do-Liste.

Klicken Sie im Dialogfeld To-Do-Liste mit der rechten Maustaste, und wählen Sie Hinzufügen. Geben Sie im Dialogfeld To-Do-Eintrag hinzufügen eine Beschreibung der Aufgabe ein, und passen die anderen Felder nach Bedarf an. Klicken Sie auf OK.

### Einfügen eines To-Do-Listeneintrags als Kommentar im Quelltext

Setzen Sie den Cursor im Quelltext-Editor an die Position, an der Sie den Kommentar hinzufügen möchten. Klicken Sie mit der rechten Maustaste, und wählen Sie „To-Do-Eintrag hinzufügen“. Wählen Sie im Dialogfeld „To-Do-Eintrag hinzufügen“ den Eintrag aus, den Sie einfügen möchten. Klicken Sie auf OK. Der Eintrag wird als Kommentar in den Quelltext eingefügt, wobei ihm das Wort TODO vorangestellt wird.

oder einfach Umschalten+ Strg + T  &#9658; { TODO : mein Text }

### Markieren von To-Do-Listeneinträgen als „erledigt“

Wählen Sie Ansicht &#9658; To-Do-Liste.

Klicken Sie im Dialogfeld „To-Do-Liste“ auf das Kontrollkästchen neben dem Eintrag, der als erledigt markiert werden soll. Der Eintrag bleibt in der Liste, wird aber mit durchgestrichenem Text angezeigt. Wenn der Eintrag dem Quelltext als Kommentar hinzugefügt wurde, wird er nun mit DONE statt mit TODO angezeigt.

### Filtern von Einträge einer To-Do-Liste

Wählen Sie Ansicht &#9658; To-Do-Liste.

Klicken Sie im Dialogfeld „To-Do-Liste“ mit der rechten Maustaste, und wählen Sie „Filter“. Wählen Sie je nach gewünschtem Filterkriterium entweder „Kategorien“, „Besitzer“ oder „Eintragstypen“ aus. Deaktivieren Sie im Dialogfeld „To-Do-Liste“ die Kontrollkästchen derjenigen Einträge, die in der To-Do-Liste ausgeblendet werden sollen. Klicken Sie auf OK. Die To-Do-Liste wird wieder angezeigt, und die gefilterten Einträge sind nicht mehr zu sehen. Die Statusleiste unten im Dialogfeld zeigt an, wie viele Einträge aufgrund des aktiven Filters verborgen werden.

### Löschen eines Eintrages aus der To-Do-Liste
Wählen Sie Ansicht &#9658; To-Do-Liste.

Markieren Sie im Dialogfeld den zu löschenden Eintrag. Klicken Sie mit der rechten Maustaste, und wählen Sie „Löschen“. Der Eintrag wird aus der To-Do-Liste gelöscht. Falls der Eintrag als Kommentar in den Quelltext eingefügt wurde, wird auch der betreffende Kommentar gelöscht.

## Regionen
Bestimmte Bereiche im Code können zu Regionen zusammengefasst werden und damit auch auf- und zugeklappt werden.

```delphi
{$REGION ‘LastStringPos‘}

function TfrmPlugInDemo.LastStringPos(const aPart, aWhole: string): Integer;

  var
    vReverse        : string;
    vRevPart        : string;
    vValue, vRetVal : integer;

  begin
    vReverse  := AnsiReverseString(aWhole);
    vRevPart  := AnsiReverseString(aPart);
    vValue    := Pos(vReverse, vRevPart);

    if vValue = 0 then
      vRetVal := -1
    else

    vRetVal := (Length(aPart) + 1) - Length(aWhole) – vValue;
    Result  := vRetVal;

  end;

{$ENDREGION}
```

# Erhöhung der Lesbarkeit

## Automatische Formatierung

Die Jedi Formatierung wurde abgelöst durch die RAD Studio interne Lösung. Hier gibt es mehr Einstellmöglichkeiten.

Kopieren von:
>Formatter_Ourplant.config

Quelle: 
>svn://192.168.0.142/svn/VicoBase.V15/configuration/Formatte/Formatter_Ourplant.config

Speichern in:
>C:\Users\%USERNAME%\AppData\Roaming\Embarcadero\BDS\12.0\

Im RAD Studio:
>Tools &#9658; Optionen &#9658; Formatierung &#9658; Profile und Status &#9658; Formatter_Ourplant.config laden!

## Verwendung des Leerzeichens

Leerzeichen sollten nicht verwendet werden:

- zwischen einem Methodennamen und seiner öffnenden Klammer,
- vor oder nach einem . (Punkt)-Operator,
- zwischen einem einstelligen Operator und seinem Operanden,
- zwischen einer Typumwandlung (cast) und dem Ausdruck, der umgewandelt werden soll,
- nach einer öffnenden Klammer oder vor einer schließenden Klammer,
- nach einer öffnenden eckigen Klammer [ oder vor einer schließenden eckigen Klammer ] oder
- vor einem Semikolon.

## Leerzeilen

Leerzeilen können die Lesbarkeit erhöhen, indem Code-Abschnitte gebildet werden, die logisch zusammengehören. Leerzeilen sollten außerdem an folgenden Stellen verwendet werden:

- Nach dem Copyright-Kommentarblock, der Package-Deklaration und dem Import-Abschnitt,
- zwischen Klassendeklarationen und
- zwischen Methodendeklarationen.

# Anweisungen
Anweisungen sind eine oder mehrere Codezeilen gefolgt von einem Strichpunkt.
Hier ist eine einfache Anweisung:

```delphi
A := B;
```

Hier ist eine zusammengesetzte (oder strukturierte) Anweisung:

```delphi
begin
  B := C;
  A := B;
end;
```

## Einfache Anweisungen

Eine einfache Anweisung enthält einen einzelnen Strichpunkt. Wenn es nötig ist, die Anweisung auf mehrere Zeilen zu verteilen, rücken Sie die zweite Zeile zwei Leerstellen mehr ein als die vorherige:

```delphi
MyValue := MyValue + (SomeVeryLongStatement / OtherLongStatement);
```

## Zusammengesetzte Anweisungen

Zusammengesetzte Anweisungen enden immer mit einem Strichpunkt, außer sie stehen direkt vor einer end-Anweisung. In diesem Fall wäre der Strichpunkt optional.

## Zuweisungen und Ausdrucksanweisungen

Jede Zeile sollte höchstens eine Anweisung beinhalten, z.B.:

**<span style="color:red">BAD STYLE:</span>**

```delphi
A := B + C; Inc(Count);
```

**<span style="color:green">GOOD STYLE:</span>**

```delphi
A := B + C;
Inc(Count);
```

## Deklaration lokaler Variablen

Lokale Variablen sollten in Camel Caps benannt werden und bekommen ein v vorangestellt.

```delphi
var
  vArraySize, vArrayCount: Integer;
```

## Array-Deklarationen

Hier sollte immer vor der geöffneten Klammer “[" und nach der schließenden "]” ein Leerzeichen stehen.

```delphi
type
  TMyArray = array [0..100] of Char;
```

## Verzweigungen und Schleifen

if-Anweisungen sollten immer mindestens in zwei Zeilen stehen. 

**<span style="color:red">BAD STYLE:</span>**

```delphi
if A < B then DoSomething;
```

**<span style="color:green">GOOD STYLE:</span>**

```delphi
if A < B then
  DoSomething;
```

Setze in zusammengesetzten if-Anweisungen jedes Element, das Anweisungen voneinander trennt in eine neue Zeile:

**<span style="color:red">BAD STYLE:</span>**

```delphi
if A < B then begin
  DoSomething;
  DoSomethingElse;

end else begin
  Foo;
  Bar;

end;
```
 

**<span style="color:green">GOOD STYLE:</span>**

```delphi
if A < B then
  begin
    DoSomething;
    DoSomethingElse;
  end
else
  begin
    Foo;
    Bar;
  end;
```

Gleiches gilt für case-Verzweigungen, repeat-until-, while- und for-Schleifen sowie try-except-Anweisungen.

```delphi
for i := 0 to 10 do
  begin
    DoSomething;
  end;
```
 
```delphi
while x < j do
  begin
    DoSomethingElse;
  end;
```
 
```delphi
repeat
  X := j;
  UpdateValue(j);
until j > 25;
```
 
```delphi
case Control.Align of
  vLeft, vNone: NewRange := Max(NewRange, Position);
  vRight: Inc(AlignMargin, Control.Width);
end;
```
 
```delphi
try
  try
    EnumThreadWindows(CurrentThreadID, @Disable, 0);
    Result := TaskWindowList;
  except
    EnableTaskWindows(TaskWindowList);
    raise;
  end;
finally
  TaskWindowList := SaveWindowList;
  TaskActiveWindow := SaveActiveWindow;
end;
```

## Rücksprung aus Funktionen / Funktionen in Übergabeparametern.

Der Rückgabewert der Funktion wird immer in der Variable Result gespeichert. Diese muss initialisiert werden, da sonst der Rückgabewert nicht definiert ist!


# Zusammenfassung der Regeln

- Verwende die englische Sprache. Bei umgangssprachlich unüblichen englischen Wörtern kann eine deutsche Beschreibung in der Dokumentation des Sourcecodes verwendet werden.

- Beschreibung im Dateikopf muss vorhanden sein.

- Nutze Namespaces und ordne die uses-Reihenfolge mit vollqualifizierten Namen, z.B. System.Utils

- Verwende die kleinstmögliche Sichtbarkeit nach außen, z.B. strict private.

- Globale Variablen sind verboten!

- Sammle globale Konstanten in einer Datei.

- Achte auf Namenskonventionen und Präfixe f, T, v, a, c, I, E etc.

- Namen für Klassen, Methoden, Variablen, etc sollen aussagekräftig sein und ersparen Dokumentation.

- Checke den Code etwas sauberer ein als du ihn ausgecheckt hast.

- Gesetz von LeBlanc: Was du später machen willst, wirst du nie machen.

- Besser nicht dokumentieren, als schlecht dokumentieren. Der Code sollte selbsterklärend sein.

- Verwende keine Magic Numbers!

- Erhöhe die Lesbarkeit und nutze die automatische Formatierung vom RAD Studio.

- In einer Funktion muss Result zu Beginn initialisiert werden.

- Vermeide z.B. Exit(-1) um aus einer Funktion frühzeitig rauszuspringen, es sei denn, es ist eine Startprüfung in der Funktion.


 

## TODO

### Logging

Geplant ist die Loglevel für jedes einzelne Modul in einer Art Modul/Komponentenverwaltungeinstellen zu können. Dazu ist es aber auch notwendig, die entsprechenden Loginformationen in den Modulen/Komponenten mitzuschreiben. Doch wie und wo gehören welche Loginformationen hin?

| Name  | Desciption                                                                                                                                     |
| :----- | :---------------------------------------------------------------------------------------------------------------------------------------------- |
| ALL   | alle Meldungen werden ungefiltert ausgegeben                                                                                                   |
| TRACE | ausführlicheres Debugging (seit Version 1.2.12), Kommentare                                                                                    |
| DEBUG | allgemeines Debugging (auffinden von Fehlern)                                                                                                  |
| INFO  | allgemeine Informationen (Programm gestartet, Programm beendet, Verbindung zu Host Foo aufgebaut, Verarbeitung dauerte SoUndSoviel Sekunden …) |
| WARN  | auftreten einer unerwarteten Situation                                                                                                         |
| ERROR | Fehler (Ausnahme wurde abgefangen. Bearbeitung wurde alternativ fortgesetzt)                                                                   |
| FATAL | Kritischer Fehler, Programmabbruch                                                                                                             |
| OFF   | Logging ist deaktiviert                                                                                                                        |

Logging sollte weitgehend automatisiert werden.


 

# Dokumentation
mit Documentation Insight von Devjet lässt sich sehr schnell während des Programmierens auch dokumentieren.

http://www.devjetsoftware.com/products/documentation-insight/
```delphi
{$REGION 'TMyClass'}
  /// <summary>
  ///   Summary works
  /// </summary>

  /// <remarks>
  ///   Remarks works
  /// </remarks>

  /// <exception cref="www.some.link">This works</exception>

  /// <list type="bullet">
  ///   <item>
  ///     <description>description does not work</description>
  ///   </item>
  ///   <item>
  ///     <description>description does not work</description>
  ///   </item>
  /// </list>

  /// <permission cref="www.some.link">This works</permission>
  /// <example>
  ///   <code>
  ///     Code example does not work
  ///   </code>
  /// </example>
{$ENDREGION}
```

Sonstiges

```delphi
{$IFDEF DEBUG}
  ReportMemoryLeaksOnShutdown:=True;
{$ENDIF}
```