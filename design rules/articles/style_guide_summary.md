# Zusammenfassung der Regeln

- Verwende die englische Sprache. Bei umgangssprachlich unüblichen englischen Wörtern kann eine deutsche Beschreibung in der Dokumentation des Sourcecodes verwendet werden.

- Beschreibung im Dateikopf muss vorhanden sein.

- Nutze Namespaces und ordne die uses-Reihenfolge mit vollqualifizierten Namen, z.B. System.Utils

- Verwende die kleinstmögliche Sichtbarkeit nach außen, z.B. strict private.

- Globale Variablen sind verboten!

- Sammle globale Konstanten in einer Datei.

- Achte auf Namenskonventionen und Präfixe f, T, v, a, c, I, E etc.

- Namen für Klassen, Methoden, Variablen, etc sollen aussagekräftig sein und ersparen Dokumentation.

- Checke den Code etwas sauberer ein als du ihn ausgecheckt hast.

- Gesetz von LeBlanc: Was du später machen willst, wirst du nie machen.

- Besser nicht dokumentieren, als schlecht dokumentieren. Der Code sollte selbsterklärend sein.

- Verwende keine Magic Numbers!

- Erhöhe die Lesbarkeit und nutze die automatische Formatierung vom RAD Studio.

- In einer Funktion muss Result zu Beginn initialisiert werden.

- Vermeide z.B. Exit(-1) um aus einer Funktion frühzeitig rauszuspringen, es sei denn, es ist eine Startprüfung in der Funktion.


---------------------------------------
Copyright &copy; 2021 Häcker Automation 