# Namen, Namespace und Uses
RAD Studio unterstützt jetzt zusätzlich zum Namespace- oder Unit-Namen einen Unit-Gültigkeitsbereichsnamen bzw. ein Präfix. Damit ein Name als voll qualifiziert gilt, muss er den Unit-Gültigkeitsbereichsnamen enthalten.

## Units
Units werden mit einem Namespace versehen, z.B.:

```delphi
unit module.MyModuleA
```

Damit heißt die Datei auch 

>module.MyModuleA.pas

Units werden vollqualifiziert mit `uses` importiert:

```delphi
uses module.MyModuleA
```

## Schreibweise

Der Namespace wird klein geschrieben, das Modul nach der bekannten InfixCaps Schreibweise.

>common.LinkManager.pas

und für den Unittest

>test\common.test.LinkManager.pas

## Uses-Reihenfolge
Vorab sind alle unnötigen `uses`-Anweisungen zu entfernen: 
>Tools &#9658; CnPack &#9658; benutze Cleaner

Weiterhin soweit es möglich ist, die uses in den implementation Teil verschieben.

Alles uses-Anweisungen sind vollqualifiziert anzugeben in folgender Reihenfolge:

1. Units im selben Projekt
2. Units aus der Häckersoftware wie z.B. Module, Bibliotheken etc.
3. Drittanbieter, z.B. Jedi Komponenten
4. Vollqualifizierte Systemkomponenten

Pro Zeile eine uses Anweisung!

```delphi
unit Base.PlugInDemo

interface

uses

  Base.PlugInBase,  // Part of the same project.
  Lib.FolderMon,    // Unit from a module or library.
  JvPlugIn,         // Third party tools.
  JvJVCLUtils,
  JvComponentBase,
  System.SysUtils,  // Fully qualified system components.
  System.Classes,
  Vcl.Forms;
  // ...

implementation
```

## interface und implementation

Der interface-Abschnitt dient dazu, Funktionen, Prozeduren, Typen usw. dieser Unit anderen Units zur Verfügung zu stellen. Alles, was hier steht, kann von außen verwendet werden. Bei Prozeduren und Funktionen steht hier nur der Kopf der Routine (Name, Parameter und evtl. Rückgabewert). Die Definition folgt dann im Abschnitt `implementation`. Der `interface`-Abschnitt endet mit Beginn des `implementation`-Abschnitts.

## initialization und finalization
Bei Bedarf können am Ende einer Unit noch zwei Abschnitte stehen: `initialization` und `finalization`.

`initialization` muss dabei als erstes aufgeführt werden. Hier werden alle Befehle aufgeführt, die bei Programmstart der Reihe nach ausgeführt werden sollen. Danach folgt das Ende der Unit (end.) oder der `finalization`-Abschnitt. Dieser ist das Gegenstück zu `initialization`. Hier können z. B. vor Programmende Objekte freigegeben werden.

`finalization` kann nur verwendet werden, wenn es auch einen `initialization`-Abschnitt gibt; `initialization` kann jedoch auch ohne `finalization` vorkommen. Eine Unit funktioniert allerdings auch ohne diese Abschnitte.

```delphi
unit Unit1;

interface

implementation

initialization

finalization

end.
```

## Globale Variablen
Die Verwendung von globalen Variablen sollte auf jeden Fall vermieden werden.

Dies gilt auch für die Variablen, die automatisch beim Erstellen einer Form angelegt werden.

```delphi
// ...

var // Comment this out or delete it altogether, unless it’s StartForm!

  Form1: TForm1; // Same here!

// ...
```

Danach erscheint ein Fehler in der `dpr;  Application.CreateForm(TForm1, Form1);` ebenfalls löschen

## Globale Konstanten

Globale Konstanten sollten in einer zentralen Datei „gesammelt“ werden und werden in Großbuchstaben geschrieben. Wörter werden mit Unterstrichen getrennt. Konstanten beginnen immer mit dem Präfix C_

```delphi
const C_MYMODULE_START = 1;
```
---------------------------------------
Copyright &copy; 2021 Häcker Automation 