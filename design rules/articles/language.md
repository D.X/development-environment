# Sprache

Englisch wird grundsätzlich verwendet.

- Dokumentation

- Commits und Relase_*.inc Dateien

- Bemerkungen im Code

- Dateinamen

- Verzeichnisse

- Namen von Variablen und Funktionen

Bei Bezeichnern die nicht zum umgangssprachlichen Englisch gehören, kann der deutsche Bezeichner z.B. in die Doku geschrieben werden. Bsp.:

```delphi
// ...

  /// <remarks>

  /// 3D Bestückkopf

  /// </remarks>

// ...

function TForm1.GetPlacerName: string;

begin

  Result := fPlacerName;

end.
```

---------------------------------------
Copyright &copy; 2021 Häcker Automation 
