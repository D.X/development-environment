# Dateikopf

Im Dateikopf sollte immer eine Beschreibung stehen:
```delphi
{****************************************************************************************************************************
  Copyright (c) 2022 Häcker Automation GmbH. All rights reserved.
  https://haecker-automation.de
****************************************************************************************************************************}

```

Diese Vorlage steht im SVN

>svn://192.168.0.142/svn/VicoBase.V15/configuration/Vorlagen/Delphi

und muss kopiert werden nach:

>C:\Users\\**xxx**\Documents\Embarcadero\Studio\code_templates


2 Möglichkeiten den Header über die Entwicklungsumgebung einzufügen (an Cursorposition):	
1) Ansicht: Tool-Fenster: Vorlagen
   Das Vorlagen Fenster öffnet sich: **Filehead** auswählen. Der Header wird automatisch eingefügt
2) Eingabe Text: **filehead** und anschließen die Taste Tab drücken


---------------------------------------
Copyright &copy; 2021 Häcker Automation 