# Erhöhung der Lesbarkeit

## Automatische Formatierung

Die Jedi Formatierung wurde abgelöst durch die RAD Studio interne Lösung. Hier gibt es mehr Einstellmöglichkeiten.

Kopieren von:
>Formatter_Ourplant.config

Quelle: 
>svn://192.168.0.142/svn/VicoBase.V15/configuration/Formatte/Formatter_Ourplant.config

Speichern in:
>C:\Users\%USERNAME%\AppData\Roaming\Embarcadero\BDS\12.0\

Im RAD Studio:
>Tools &#9658; Optionen &#9658; Formatierung &#9658; Profile und Status &#9658; Formatter_Ourplant.config laden!

## Verwendung des Leerzeichens

Leerzeichen sollten nicht verwendet werden:

- zwischen einem Methodennamen und seiner öffnenden Klammer,
- vor oder nach einem . (Punkt)-Operator,
- zwischen einem einstelligen Operator und seinem Operanden,
- zwischen einer Typumwandlung (cast) und dem Ausdruck, der umgewandelt werden soll,
- nach einer öffnenden Klammer oder vor einer schließenden Klammer,
- nach einer öffnenden eckigen Klammer [ oder vor einer schließenden eckigen Klammer ] oder
- vor einem Semikolon.

## Leerzeilen

Leerzeilen können die Lesbarkeit erhöhen, indem Code-Abschnitte gebildet werden, die logisch zusammengehören. Leerzeilen sollten außerdem an folgenden Stellen verwendet werden:

- Nach dem Copyright-Kommentarblock, der Package-Deklaration und dem Import-Abschnitt,
- zwischen Klassendeklarationen und
- zwischen Methodendeklarationen.
---------------------------------------
Copyright &copy; 2021 Häcker Automation 