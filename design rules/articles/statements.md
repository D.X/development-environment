# Anweisungen
Anweisungen sind eine oder mehrere Codezeilen gefolgt von einem Strichpunkt.
Hier ist eine einfache Anweisung:

```delphi
A := B;
```

Hier ist eine zusammengesetzte (oder strukturierte) Anweisung:

```delphi
begin
  B := C;
  A := B;
end;
```

## Einfache Anweisungen

Eine einfache Anweisung enthält einen einzelnen Strichpunkt. Wenn es nötig ist, die Anweisung auf mehrere Zeilen zu verteilen, rücken Sie die zweite Zeile zwei Leerstellen mehr ein als die vorherige:

```delphi
MyValue := MyValue + (SomeVeryLongStatement / OtherLongStatement);
```

## Zusammengesetzte Anweisungen

Zusammengesetzte Anweisungen enden immer mit einem Strichpunkt, außer sie stehen direkt vor einer end-Anweisung. In diesem Fall wäre der Strichpunkt optional.

## Zuweisungen und Ausdrucksanweisungen

Jede Zeile sollte höchstens eine Anweisung beinhalten, z.B.:

**<span style="color:red">BAD STYLE:</span>**

```delphi
A := B + C; Inc(Count);
```

**<span style="color:green">GOOD STYLE:</span>**

```delphi
A := B + C;
Inc(Count);
```

## Deklaration lokaler Variablen

Lokale Variablen sollten in Camel Caps benannt werden und bekommen ein v vorangestellt.

```delphi
var
  vArraySize, vArrayCount: Integer;
```

## Array-Deklarationen

Hier sollte immer vor der geöffneten Klammer “[" und nach der schließenden "]” ein Leerzeichen stehen.

```delphi
type
  TMyArray = array [0..100] of Char;
```

## Verzweigungen und Schleifen

if-Anweisungen sollten immer mindestens in zwei Zeilen stehen. 

**<span style="color:red">BAD STYLE:</span>**

```delphi
if A < B then DoSomething;
```

**<span style="color:green">GOOD STYLE:</span>**

```delphi
if A < B then
  DoSomething;
```

Setze in zusammengesetzten if-Anweisungen jedes Element, das Anweisungen voneinander trennt in eine neue Zeile:

**<span style="color:red">BAD STYLE:</span>**

```delphi
if A < B then begin
  DoSomething;
  DoSomethingElse;

end else begin
  Foo;
  Bar;

end;
```
 

**<span style="color:green">GOOD STYLE:</span>**

```delphi
if A < B then
  begin
    DoSomething;
    DoSomethingElse;
  end
else
  begin
    Foo;
    Bar;
  end;
```

Gleiches gilt für case-Verzweigungen, repeat-until-, while- und for-Schleifen sowie try-except-Anweisungen.

```delphi
for i := 0 to 10 do
  begin
    DoSomething;
  end;
```
 
```delphi
while x < j do
  begin
    DoSomethingElse;
  end;
```
 
```delphi
repeat
  X := j;
  UpdateValue(j);
until j > 25;
```
 
```delphi
case Control.Align of
  vLeft, vNone: NewRange := Max(NewRange, Position);
  vRight: Inc(AlignMargin, Control.Width);
end;
```
 
```delphi
try
  try
    EnumThreadWindows(CurrentThreadID, @Disable, 0);
    Result := TaskWindowList;
  except
    EnableTaskWindows(TaskWindowList);
    raise;
  end;
finally
  TaskWindowList := SaveWindowList;
  TaskActiveWindow := SaveActiveWindow;
end;
```

## Rücksprung aus Funktionen / Funktionen in Übergabeparametern.

Der Rückgabewert der Funktion wird immer in der Variable Result gespeichert. Diese muss initialisiert werden, da sonst der Rückgabewert nicht definiert ist!

---------------------------------------
Copyright &copy; 2021 Häcker Automation 