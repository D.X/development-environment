# Tipps

Checke den Code etwas sauberer ein als du ihn ausgecheckt hast. Immer funktionelle Änderungen und Code-Aufräumarbeiten getrennt einchecken, sonst wird es unübersichtlich!

Gesetz von LeBlanc: Was du später machen willst, wirst du nie machen.

Besser nicht dokumentieren, als schlecht dokumentieren. Der Code sollte selbsterklärend sein.

Aussagekräftige Namen verwenden!

**Hinweis:** Die meisten nachfolgenden Beispiele entsprechen sehr oft nicht unseren Richtlinien. Es sollen hier lediglich bestimmte „schlechte“ Programmierstile verdeutlicht werden und wie man diese am besten vermeidet.

**Bad variable naming:**

```delphi
int etd;

int dsc;

int dsm;

int fad;
```
 

**Good variable naming:**

```delphi
int ElapsedTimeInDays;

int DaysSinceCreation;

int DaysSinceModification;

int FileAgeInDays;
```

 

**<span style="color:red">BAD STYLE:</span>**
```delphi
public List<int[]> getThem() {
       List<int[]> list1 = new ArrayList<int[]>();

for (int[] x : theList) {
              if (x[0] == 4) {
                    list1.add(x);
              }
}
       return list1;
}
```
 

**<span style="color:green">GOOD STYLE:</span>**
```delphi
public List<int[]> getFlaggedCells() {
       List<int[]> flaggedCells = new ArrayList<inst[]>();

       for (int[] cell : gameBoard) {
              if (cell[STATUS_VALUE] == FLAGGED) {
                    flaggedCells.add(cell);
              }
       }
       return flaggedCells;
}
```
 

**<span style="color:red">BAD STYLE:</span>**
```delphi
Class DtaRcrd102 {
       Private Date genymdhms;
       Private Date modymdhms;
       private final String pszqint = „102“;
       /* ... */
}
```
 

**<span style="color:green">GOOD STYLE:</span>**
```delphi
Class Customer {
       Private Date generationTimestamp;
       Private Date modificationTimestamp;
       Private final String recordId = „102“;
       /* ... */
}
```

## Magic Numbers

Anstelle von aussagekraftlosen Zahlen sollten lokale Konstanten verwendet werden. Ausnahme: Zugriff auf Listenelement [0] o.ä.

**<span style="color:red">BAD STYLE:</span>**

```delphi
if data.length < 1048576 then
begin
  // ...
end;

if status = 2 then
begin
  // ...
end;
```
 
**<span style="color:green">GOOD STYLE:</span>**
```delphi
if datalength < C_MAX_FILE_SIZE_KILOBYTES then
begin
  // ...
end;

if status = C_STATUS_ACTIVE then
begin
  // ...
end;
```

## To-Do-Listen verwenden

http://docwiki.embarcadero.com/RADStudio/XE3/de/To-Do-Listen_verwenden

In einer To-Do-Liste werden die Programmierarbeiten eingetragen und angezeigt, die in einem Projekt noch ausgeführt werden müssen.

### Erstellung einer To-Do-Liste / Hinzufügen von Einträgen
Wählen Sie Ansicht &#9658; To-Do-Liste.

Klicken Sie im Dialogfeld To-Do-Liste mit der rechten Maustaste, und wählen Sie Hinzufügen. Geben Sie im Dialogfeld To-Do-Eintrag hinzufügen eine Beschreibung der Aufgabe ein, und passen die anderen Felder nach Bedarf an. Klicken Sie auf OK.

### Einfügen eines To-Do-Listeneintrags als Kommentar im Quelltext

Setzen Sie den Cursor im Quelltext-Editor an die Position, an der Sie den Kommentar hinzufügen möchten. Klicken Sie mit der rechten Maustaste, und wählen Sie „To-Do-Eintrag hinzufügen“. Wählen Sie im Dialogfeld „To-Do-Eintrag hinzufügen“ den Eintrag aus, den Sie einfügen möchten. Klicken Sie auf OK. Der Eintrag wird als Kommentar in den Quelltext eingefügt, wobei ihm das Wort TODO vorangestellt wird.

oder einfach Umschalten+ Strg + T  &#9658; { TODO : mein Text }

### Markieren von To-Do-Listeneinträgen als „erledigt“

Wählen Sie Ansicht &#9658; To-Do-Liste.

Klicken Sie im Dialogfeld „To-Do-Liste“ auf das Kontrollkästchen neben dem Eintrag, der als erledigt markiert werden soll. Der Eintrag bleibt in der Liste, wird aber mit durchgestrichenem Text angezeigt. Wenn der Eintrag dem Quelltext als Kommentar hinzugefügt wurde, wird er nun mit DONE statt mit TODO angezeigt.

### Filtern von Einträge einer To-Do-Liste

Wählen Sie Ansicht &#9658; To-Do-Liste.

Klicken Sie im Dialogfeld „To-Do-Liste“ mit der rechten Maustaste, und wählen Sie „Filter“. Wählen Sie je nach gewünschtem Filterkriterium entweder „Kategorien“, „Besitzer“ oder „Eintragstypen“ aus. Deaktivieren Sie im Dialogfeld „To-Do-Liste“ die Kontrollkästchen derjenigen Einträge, die in der To-Do-Liste ausgeblendet werden sollen. Klicken Sie auf OK. Die To-Do-Liste wird wieder angezeigt, und die gefilterten Einträge sind nicht mehr zu sehen. Die Statusleiste unten im Dialogfeld zeigt an, wie viele Einträge aufgrund des aktiven Filters verborgen werden.

### Löschen eines Eintrages aus der To-Do-Liste
Wählen Sie Ansicht &#9658; To-Do-Liste.

Markieren Sie im Dialogfeld den zu löschenden Eintrag. Klicken Sie mit der rechten Maustaste, und wählen Sie „Löschen“. Der Eintrag wird aus der To-Do-Liste gelöscht. Falls der Eintrag als Kommentar in den Quelltext eingefügt wurde, wird auch der betreffende Kommentar gelöscht.

## Regionen
Bestimmte Bereiche im Code können zu Regionen zusammengefasst werden und damit auch auf- und zugeklappt werden.

```delphi
{$REGION ‘LastStringPos‘}

function TfrmPlugInDemo.LastStringPos(const aPart, aWhole: string): Integer;

  var
    vReverse        : string;
    vRevPart        : string;
    vValue, vRetVal : integer;

  begin
    vReverse  := AnsiReverseString(aWhole);
    vRevPart  := AnsiReverseString(aPart);
    vValue    := Pos(vReverse, vRevPart);

    if vValue = 0 then
      vRetVal := -1
    else

    vRetVal := (Length(aPart) + 1) - Length(aWhole) – vValue;
    Result  := vRetVal;

  end;

{$ENDREGION}
```
---------------------------------------
Copyright &copy; 2021 Häcker Automation 