# Namenskonventionen

Außer bei reservierten Wörtern und Direktiven, die in Kleinbuchstaben sind, sollten alle Pascal-Bezeichner Camel Caps verwenden, was bedeutet, dass der erste Buchstabe ein Großbuchstabe und jedes in einem Bezeichner enthaltene Wort ebenso wie eingebettete Abkürzungen in Großbuchstaben sein sollten.

`MyIdentifier`
`MyFTPClass`

## Typen wie records, enums, etc

… beginnen mit einem großen T.
```delphi
type

  TColor = (Black, Yellow, Red);

  // Enumeration values, Mon = 1, Tue = 2, ..., Sun = 7.

  TDay = (Mon = 1, Tue, Wed, Thu, Fri, Sat, Sun);

  // Enumeration values, Hearts = 13, Diamonds = 14, Clubs = 22, Spades = 23.

  TSuit = (Hearts=13, Diamonds, Clubs = 22, Spades);

  TEmployee = record

    Name: string;

end;
```

## Klassen-Benennung
Klassennamen bekommen als Präfix ein T

`TFolderMon = class;`

### Sichtbarkeit
| <div style="width:150px">Name</div> | Beschreibung                                                                                                                                                                                     |
| :---------------------------------- | :----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| `strict private`                    | Elemente sind nur innerhalb der Klasse sichtbar. Andere Klassen können nicht darauf zugreifen, auch wenn sie sich in derselben Unit befinden.                                                    |
| `strict protected`                  | Elemente sind innerhalb der Klasse und in allen davon abgeleiteten Klassen sichtbar, jedoch nicht in anderen (nicht verwandten) Klassen derselben                                                |
| `private`                           | Elemente können nur innerhalb der gleichen Unit verwendet werden. Aus anderen Units ist ein Zugriff nicht möglich.                                                                               |
| `protected`                         | Elemente sind wie private-Elemente innerhalb der gleichen Unit verwendbar. Darüberhinaus haben alle abgeleiteten Klassen darauf Zugriff, unabhängig davon, in welcher Unit sie sich befinden.    |
| `public`                            | Elemente unterliegen keinen Zugriffsbeschränkungen.                                                                                                                                              |
| `published`                         | Elemente haben dieselbe Sichtbarkeit wie public-Elemente. Zusätzlich werden diese Element im Objektinspektor angezeigt, weshalb nicht alle Typen als published-Element eingesetzt werden können. |

Es wird empfohlen mit strict zu arbeiten!

```delphi
type

  TfrmPlugInDemo = class(TfrmPlugInBase)
  strict private
  { Strict Private declarations }

  strict protected
  { Strict Protected declarations }

  private
  { Private-Deklarationen }

  protected
  { Protected -Deklarationen }

  public
  { Public-Deklarationen }

  published
  { Published-Deklarationen }

end;
```

### Benennung von Attributen
Verwendung von Camel Caps. Jede Typdeklaration beginnt mit einem kleinen f

Deklariere alle Datentypen im private-Abschnitt und verwende Properties oder Get- und Set-Methoden für den Zugriff darauf.

**Correct:**
```delphi
fMyString: string;
```

**Incorrect:**
```delphi
lpstrMyString: string;
```

### Methoden-Benennung
Für Methodennamen ist ebenfalls der Camel Caps-Stil zu verwenden. Das erste Wort beginnt bei `public`-Methoden mit einem Großbuchstaben, sonst mit einem Kleinbuchstaben. Jedes weitere Wort beginnt mit einem Großbuchstaben, Abkürzungen werden komplett groß geschrieben. Alle weiteren Buchstaben sind kleine. Namen sollten kurze, bezeichnende Sätze sein.

**GOOD method naming:**
```delphi
ShowStatus, DrawCircle, AddLayoutComponent
```
 

**BAD method naming:**
```delphi
MouseButton           // Not a sentence, does not describe what the method does.

add_layout_component  // Underscores are not allowed.

ServerRunning         // What does this do? Does it check if the server is running

                      // (better name would be: IsServerRunning)?

                      // Does it start the server (better name: StartServer)?
```

### Konstruktoren, Destruktoren, etc.

Beim Instanziieren einer Klasse werden verschiedene Prozeduren durchlaufen. Diese Stellen sollten verwendet werden um andere Klassen, Variablen etc. zu instanziieren, initialisieren, zerstören, usw.

Loaded &#9658; Create &#9658; AfterConstruction &#9658; … &#9658; BeforeDestruction &#9658; Destroy

```delphi
protected

  procedure Loaded; override;

public

  constructor Create(aOwner: TComponent); override;

  procedure AfterConstruction; override;

  procedure BeforeDestruction; override;

  destructor Destroy; override;
```

## Setter und Getter

Der Zugriff auf Klassenattribute sollte nie unmittelbar geschehen, sondern durch Get- und Set-Methoden.

```delphi
private

  var

    fHeight: Real;

public

  procedure SetHeight(aNewHeight: Integer);

  function GetHeight: Integer;
```

## Boolean

Um boolesche Werte abzufragen, werden Is-Funktionen verwendet.

```delphi
private

  var

    fResizable: Boolean;

public

  function IsResizable: Boolean;
```

## Properties

… haben keinen Präfix.

```delphi
type

  TCar = class

  private

    fColor: TColor;

    procedure SetMyColor (const Color: TColor);

  public

    property Color: TColor read fColor write SetMyColor;

  end;
```

## Interfaces

Interfaces bekommen den Präfix I. Ansonsten folgen sie den Regeln für Klassendefinitionen.

```delphi
type

  IMath = interface(IInterface)

  function Add(aSum1, aSum2: Integer): Integer;

  function Sub(aSub1, aSub2: Integer): Integer;

end
```

### Interface-Deklarationen

Interfaces werden entsprechend den Klassendeklarationen deklariert:

```delphi
IMyInterface = interface

  // Interface body.

end;
```

In einer Interface-Deklaration gibt es keine Felder, Properties dagegen sind erlaubt. Alle Interface-Methoden sind public und abstract; verwenden Sie diese Schlüsselwörter nicht extra in der Deklaration einer Interface-Methode.

### Anordnung des Interface-Hauptteils

Der Hauptteil einer Interface-Deklaration sollte in folgender Reihenfolge geordnet sein:

1. Interface-Methodendeklarationen
2. Interface-Eigenschaftendeklarationen

Die Deklarationsstile von Interfaceeigenschaften und -methoden sind mit den Stilen von Klasseneigenschaften und -methoden identisch.

## Exceptions

Exceptions haben den Präfix `E`

```delphi
EMathError = class(Exception)
```

## Lokale Konstanten

Lokale Konstanten haben den Präfix `C_` und bestehen komplett aus Großbuchstaben bzw. Unterstrichen zur Worttrennung. Sie sollten anstatt von „Magic Numbers“ eingesetzt werden.

## Lokale Variablen

Lokale Variablen haben den Präfix `v`, Übergabeparameter `a` davorgesetzt und sind `const`.

**Ausnahmen:** Variablen für Schleifen.

```delphi
procedure TMyClass.SetName(const aName, aFirstName: string);

  var
    vSurName  : string;
    I         : Integer;

  const
    C_MAX_LOOP_INDEX = 9;

  begin
    fFirstName  := aName;
    SurName     := ‘Müller’;

    for I := 0 to C_MAX_LOOP_INDEX do
    // ...
```

 

## Präfixe von GUI-Elementen

| Typ          | Präfix        | Beispiel       |
| :----------- | :------------ | :------------- |
| TForm        | (kein)        | Form1          |
| TFrame       | frm           | frmFrame1      |
| TMainMenu    | mm            | mmMenu1        |
| TLabel       | l (kleines L) | lMyLabel       |
| TEdit        | e             | eName          |
| TMemo        | mmo           | mmoTextField   |
| TButton      | b             | bOK            |
| TCheckBox    | chk           | chkCheckbox1   |
| TRadioButton | rb            | rbRadioButton1 |
| TListBox     | lst           | lstList1       |
| TComboBox    | cb            | cbComboBox1    |
| TScrollBar   | sb            | sbVertical     |
| TGroupBox    | gb            | gbGroupBox1    |
| TRadioGroup  | rg            | rgRadioGroup1  |
| TPanel       | p             | pPanel1        |
| TBitBtn      | bi            | biBitButton1   |
| TToolBar     | tb            | tbToolBar1     |
| TTimer       | tmr           | tmrTimer1      |
| TPopupMenu   | pm            | pmPopup1       |
 
---------------------------------------
Copyright &copy; 2021 Häcker Automation 