# Required Tools

For developing OurPlantOS you will need the following tools:

## Mandatory Tools
|Name|License|
|:---|---:|
|Embarcadero Delphi 10.3 | Commercial|
|[JEDI (JEDI Code Library)](https://github.com/project-jedi/jcl)|MPL 1.1|
|[JEDIVCL (JEDIVCL (JEDI Visual Component Library)](https://github.com/project-jedi/jvcl)|MPL 1.1|
|[Delphi.Mocks](https://github.com/VSoftTechnologies/Delphi-Mocks)|Apache License 2.0|
|[Spring4D](https://bitbucket.org/sglienke/spring4d/src/master/)|Apache License 2.0|
|OPDarwin|MIT / OurPlant|
|Delphi-Modbus|MPL 1.1|

## Optional Tools

|Name|License|
|:---|---:|
|SDAC (SQL Server Data Access Components)|commercial|
|EurekaLog|commercial|

You will find the installation guide [here](https://gitlab.com/ourplant.net/ourplant_os/documents/development-environment/-/blob/main//required%20tools/install%20OurPlantOS%20DevEnvironment.md).

For easier documentation in markdown you can use [these tools](https://gitlab.com/ourplant.net/ourplant_os/documents/development-environment/-/blob/main/required%20tools/ToolsForDocumentations.md)

<!-- --------------------------------------- -->
<!-- Copyright &copy; 2021 Häcker Automation  -->