## Installation von EurekaLog

identisch unter XE5 und 10.3

---

**HINWEIS:** kann NUR mit bestehender kommerzieller Lizenz installiert werden

---

**1) Alle Embarcadero Instanzen schließen**

**2) [optional] Deinstallation alter Eurekalog-Installationen**

Das Deinstallieren von alten Eurekalog-Installationen muss vorher durchgeführt werden, da dies sonst Konflikte verursacht.

**3) Eurekalog 7.9.2.0 installieren**

-   EurekaLog_7.9.2.0_Professional_for_RAD_Studio.exe starten

-   mit entsprechenden Account-Daten einloggen und installieren

**4) Aktivieren**

`Projekt öffnen` 
&rarr; `Projekt + EurekaLog options...` 
&rarr; `Enable Eurekalog for this Project` 
&rarr; `Import` 
&rarr; `Import eines Konfigurationsfiles`

**Hinweis:** für die Projekt OPDataServer und OurPlantOS.Tests **NICHT** einstellen

<!-- ![](/supplementary/pictures/image8.png) ![](/supplementary/pictures/image9.png) -->

**6) Deaktivieren von Eurekalog**

Eurekalog ist keine notwendige Bedingung für das Kompilieren von OurPlant OS und kann deaktiviert werden.

<!-- ![](/supplementary/pictures/image11.png) -->

**HINWEIS:** Nach einem Aktivieren oder Deaktivieren ist das komplette Projekt neu zu bauen!

**6) Einchecken der Projektdatei dpr**

Wenn die Projektdatei OurPlanOS.dpr mit eingecheckt wird, muss darauf geachtet werden, dass folgende Einträge erhalten bleiben! (als erste uses Anweisung)

**für Buildserver: Nbuild, Test und Release notwendig**

```delphi
{\$IFDEF EurekaLog}

EMemLeaks,

EResLeaks,

EDebugExports,

EFixSafeCallException,

EMapWin32,

EAppVCL,

**EAppNonVisual**,

ExceptionLog7,

{\$ENDIF EurekaLog}

=> entspricht z.B. der Konfiguration von OP_Eureka_Nbuild.eof

Wenn OurPlantOS.eof verwendet wird, unterscheiden sich die Einträge:

{\$IFDEF EurekaLog}

EMemLeaks,

EResLeaks,

EDebugExports,

EFixSafeCallException,

EMapWin32,

EAppVCL,

**EDialogWinAPIMSClassic,**

**EDialogWinAPIEurekaLogDetailed,**

**EDialogWinAPIStepsToReproduce,**

ExceptionLog7,

{\$ENDIF EurekaLog}
```

=> Diese nicht einchecken. Der obere Part ist richtig!
