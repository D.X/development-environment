# Documentation within the OurPlant GitLab<!-- omit in toc -->

This is a collection of ressources and tools used for documentation. Every  documentation should be done using markdown.

#### Table of Content<!-- omit in toc -->
- [Editor](#editor)
- [Document Transformation](#document-transformation)
- [Version Control](#version-control)
- [Syntax, Manuals and more](#syntax-manuals-and-more)

---
## Editor

Choose your favorite markdown editor to create or edit the documents. If you do not have any favorite editor, we recommend the following.

#### Visual Studio Code<!-- omit in toc -->

[VS Code](https://code.visualstudio.com/)

This Microsoft text editor is our main editor for our advanced users and we are using the following VS Code extensions. The main advantage of this editor is its integration of a coding system. For instance, the git integration can save a lot of effort and the vs code extensions can be used for individualization.

We recommend the following VS Code Extensions for Documentation:

-  Markdown Preview enhancer
-  Markdown All in One
-  XML
-  JSON
-  (Python)
-  (Pascal)

#### Mark Text<!-- omit in toc -->

For the beginners or people who are not so familiar with coding, we recommend the following Editor:

[Mark Text](https://marktext.app/)

This is a WYSIWYG-Editor and provides a lot of convenience feature for writing in markdown.

---
## Document Transformation

#### Pandoc<!-- omit in toc -->

##### Purpose<!-- omit in toc -->
This tool can be used for transformations between differnt formats. Especially for the transformation of larger documents to markdown this tool is quite useful.

##### Links<!-- omit in toc -->
[Pandoc - Command-Line Tool](https://pandoc.org/installing.html)
[Pandoc - GUI(requires Command-Line Tool)](https://github.com/mb21/panwriter/releases)

---
## Version Control

Every OurPlant document has to be put in its appropriate repository. Every commit has to be complied with the rules and workflows of the OurPlant-GitLab-Repos.s

---
## Syntax, Manuals and more

####  Markdown References<!-- omit in toc -->
[Markdown Cheatsheet](https://www.markdownguide.org/assets/markdown-cheat-sheet.md)

####  Git References<!-- omit in toc -->
[Git Cheatsheet](http://git-cheatsheet.com/)

####  Pandoc References<!-- omit in toc -->
[Pandoc Documentation](https://pandoc.org/MANUAL.html)

<!-- --------------------------------------- -->
<!-- Copyright &copy; 2021 Häcker Automation  -->
