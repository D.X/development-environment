# Installation Entwicklungsumgebung DX10.3                                         

## Abhängigkeiten, Bezugsquellen, Konfiguration

|||
|----:|-----|
|**erstellt von:**|Navimatix GmbH
|**Autor(en):** |Matthias Heunecke
|**Initiale Version:** |08.10.2020
|**Verantwortung:** |Häcker Automation GmbH


## Inhaltsverzeichnis

- [Installation Entwicklungsumgebung DX10.3](#installation-entwicklungsumgebung-dx103)
  - [Abhängigkeiten, Bezugsquellen, Konfiguration](#abhängigkeiten-bezugsquellen-konfiguration)
  - [Inhaltsverzeichnis](#inhaltsverzeichnis)
  - [Zweck des Dokumentes](#zweck-des-dokumentes)
  - [Ausgangspunkt](#ausgangspunkt)
  - [Bekannte Probleme](#bekannte-probleme)
- [Zielstellung](#zielstellung)
  - [Vorteile der neuen Umgebung](#vorteile-der-neuen-umgebung)
  - [Abhängigkeiten von OurPlantOS](#abhängigkeiten-von-ourplantos)
    - [Jedi (JEDI Code Library)](#jedi-jedi-code-library)
    - [JediVCL (JEDI Visual Component Library)](#jedivcl-jedi-visual-component-library)
  - [Delphi.Mocks](#delphimocks)
  - [Spring4D](#spring4d)
  - [OPDarwin](#opdarwin)
  - [Delphi-Modbus](#delphi-modbus)
  - [Web.Win.Sockets](#webwinsockets)
  - [SDAC (SQL Server Data Access Components)](#sdac-sql-server-data-access-components)
  - [Delphi IDE Colorizer (VCL Styles Utils)](#delphi-ide-colorizer-vcl-styles-utils)
  - [EurekaLog](#eurekalog)
- [Umstellung auf Packages](#umstellung-auf-packages)
- [Sonstiges](#sonstiges)

## Zweck des Dokumentes

Dieses Dokument dient der Dokumentation der Umstellung der bisherigen
Entwicklungsumgebung, Delphi XE5, von Häcker Automation GmbH, hin zu der
Version Delphi 10.3 Rio (DX10.3).

Dabei müssen auch die Anhängigkeiten (Drittabhängigkeiten) in den
Häcker-eigenen Programmprojekten umgestellt werden. Während einer
solchen Umstellung sind normalerweise auch neue Versionen von Frameworks
oder Bibliotheken zu beachten und zu aktualisieren.

Die hier skizzierten Schritte sollen den einzelnen Entwickler
unterstützen, eine vollständig nutzbare Umgebung für die Entwicklung bei
Häcker Automation zu erstellen und konfigurieren.

## Ausgangspunkt

Der Ausgangspunkt für die Umstellung ist die aktuell im Einsatz
befindliche Entwicklungsumgebung auf der Basis von Delphi XE5. Alle mit
Delphi erstellten Softwareteile werden mit der Version XE5 erstellt,
insbesondere das OurPlantOS und seine Test- und Zusatzprojekte.

Die Verwendung von Packages findet weitestgehend nicht statt, alle
notwendigen Quelltextdateien werden jedem einzelnen Delphi-Projekt
zugeordnet und darin gebaut.

Für das Projekt „OurPlantOS.exe" gilt:

-   Über 770000 LOC (Quelltextzeilen nach Ausgabe des DX10.3 Compilers)
-   Hinweise #13, Warnungen #64 -- das ist erfreulich wenig
-   Formulare #479 sind im Projekt
-   Fast 1500 Pascaldateien (.pas), das bezieht sich auf den Ordner
    „trunk", mit Tests
-   46 Unterordner auf der Hauptebene zur Strukturierung der Komponenten
-   Viele Unterordner folgen denen auf der Hauptebene
-   Unit-Abhängigkeiten:
    -   Irgendwie alles von allem abhängig

## Bekannte Probleme

Die Analyse des Abhängigkeitsgraphen der ‚Unit-Abhängigkeiten für
„OurPlantOS.exe" ergibt:

-   1262 Knoten (verwendete Units im Projekt)
-   10643 Kanten (Verbindungen zwischen den Units, Unitreferenzen)
-   Ist zyklisch und nicht planar, also Units verwenden sich gegenseitig

Durch die extreme Abhängigkeit der Units untereinander und die Tatsache,
dass alle Units den einzelnen Projekten zugeordnet sind, müssen je
Buildvorgang eine sehr große Anzahl von Quellcodezeilen (ca. 770k)
kompiliert werden (da keine Packages verwendet werden).

Der XE5-Compiler wird daher mit einem Speicherverbrauch von mehr als 1,3
GB RAM an der oberen Grenze betrieben. Daher können mehrere vollständige
Compilevorgänge (Projekt erstellen) aufgrund erschöpfter Ressourcen zu
Fehlern führen.

Durch das kaskadierte Bauen aller Quellen in einem Projekt und dessen
Abhängigkeiten kommt es immer wieder zum Fehler „F2051: Unit %s was
compiled with a different version of %s".

Die Drittabhängigkeiten sind nicht jedem Entwickler genau klar und
unterschiedlich in die IDE eingebunden. Die zugehörige Dokumentation ist
veraltet.

# Zielstellung

Das Ziel ist es, mit der Umstellung auf die neuere Version Delphi 10.3
Rio, dem Entwickler bei Häcker die tägliche Arbeit zu erleichtern.
Darüber hinaus ist diese neuere Version der IDE mit samt der zugehörigen
Compiler besser auf moderne Betriebssysteme zugeschnitten und birgt
nicht das Risiko einer Inkompatibilität mit zukünftigen Windows-Updates.

Bei der Umstellung sollen alle Drittabhängigkeiten mit auf die neuere
Umgebung umgezogen werden und gegebenenfalls durch neuere Versionen
ersetzt werden. Manche dieser Abhängigkeiten müssen sogar aktualisiert
werden, da deren Aufbau mit den Bibliotheken von DX10.3 (RTL, VCL)
inkompatibel sind.

Die Art der Einbindung von Bibliotheken und Frameworks soll bei dieser
Gelegenheit auf den Prüfstand und verbessert werden. Ziel ist die
Einbindung als DCU-Dateien, damit nur der Linker auf bereits erstellte
Quellen zugreifen muss. Ebenso sollen diese plattformneutral und
konfigurationsneutral eingebunden werden, also die Kombinationen Win32-
Debug/Release, sowie Win64- Debug/Release problemlos durch einfaches
Umschalten gebaut werden können.

## Vorteile der neuen Umgebung

Die neuere Version der IDE, samt dem zugehörigen Compiler, ist besser
auf moderne Betriebssysteme zugeschnitten und birgt nicht das erhöhte
Risiko einer Inkompatibilität mit zukünftigen Windows-Updates. Ferner
kann mal als Vorteile anführen:

-   Die win32/win64 Compiler können mit einem Speicherverbrauch von weit
    mehr als 2 GB umgehen und sind damit deutlich robuster gegenüber
    mangelnden Ressourcen.

-   Die IDE ist moderner, robuster und hat das Potential die
    Entwicklereffektivität zu steigern.

-   Alle aktuell gängigen Tools zur Optimierung der IDE können damit
    genutzt werden, etwa CnPack und MMX (Im XE5 stehen dafür, wenn
    überhaupt, nur alte Versionen bereit).

-   Mit DX10.3 werden weitere Plattformen erschlossen. Es könnten bei
    Bedarf Apps für Android und iOS, sowie Services für Linux64bit und
    OSX erstellt werden.

-   Durch die Verwendung ist die Entwicklung näher am Support von
    Embarcadero, für DX10.3 wird es noch nötige Updates geben. Ein
    Umstieg auf die jeweils neueste Version von Delphi (aktuell DX10.4
    Sydney) ist damit nur ein kleiner Schritt. Das Nutzen der
    Subscriptions, um jeweils immer die neueste Delphi-Hauptversion zu
    nutzen, ist von hier aus möglich.

## Abhängigkeiten von OurPlantOS

Die folgenden Abhängigkeiten zu Drittanbietern und externen Projekten
hat das Projekt „OurPlantOS.exe" zum aktuellen Zeitpunkt (08.10.2020):

![](media/image2.jpg){width="6.3in" height="2.204861111111111in"}

### Jedi (JEDI Code Library)

> The JEDI Code Library (JCL) consists of a set of thoroughly tested and
> fully documented utility functions and non-visual classes which can be
> instantly reused in your Delphi and C++ Builder projects.
>
> The library is built upon code donated from the JEDI community. It is
> reformatted to achieve a common look-and-feel, tested, documented and
> merged into the library. The library is grouped into several
> categories such as Strings, Files and I/O, Security, Math and many,
> many more. The library is released to the public under the terms of
> the Mozilla Public License (MPL) and as such can be freely used in
> both freeware/shareware, opensource and commercial projects. The
> entire JEDI Code Library is distributed under the terms of the Mozilla
> Public License (MPL).
>
> This includes, but is not limited to, this document and all source
> code and ancillary files. Source code files included in the JCL have a
> header which explicitly states this (as is required) however, unless
> noted otherwise, all files including those without an MPL header, are
> subject to the MPL license.

https://github.com/project-jedi/jcl

### JediVCL (JEDI Visual Component Library)

> JVCL is a library of over 600 Delphi visual and non-visual
> Delphi/C++Builder components. It supports Delphi/C++Builder 6 and
> newer.
>
> The library is built upon code donated from the JEDI community. It is
> reformatted to achieve a common look-and-feel, tested and merged into
> the library. The library is grouped into several packages. It is
> released to the public under the terms of the Mozilla Public License
> (MPL) and as such can be freely used in both freeware/shareware,
> opensource and commercial projects. The entire JEDI Visual Component
> Library is distributed under the terms of the Mozilla Public License
> (MPL).
>
> This includes, but is not limited to, this document and all source
> code and ancillary files. Source code files included in the JVCL have
> a header which explicitly states this (as is required) however, unless
> noted otherwise, all files including those without an MPL header, are
> subject to the MPL license.

https://github.com/project-jedi/jvcl

## Delphi.Mocks

> Delphi Mocks is a simple mocking framework for Delphi XE2 or later. It
> makes use of RTTI features that are only available in Delphi XE2,
> however I do hope to be able to get it working with earlier versions
> of Delphi (2010 or later) at some stage.

https://github.com/VSoftTechnologies/Delphi-Mocks

## Spring4D

> Spring4D is an open-source code library for Embarcadero Delphi 2010
> and higher. It consists of a number of different modules that contain
> a base class library (common types, interface-based collection types,
> reflection extensions) and a dependency injection framework. It uses
> the Apache License 2.0.

https://bitbucket.org/sglienke/spring4d/src/master/

## OPDarwin

```delphi
/// \<summary>
/// \<para>
/// Design Rules
/// \</para>
/// \<para>
/// - use only the following defined colors \<br/>\<br/>- avoid obsolete
/// colors, they are only for old stlyed elements \<br/>\<br/>- do not
/// change colors ( IOPColorAdministration Interface ).
/// IOPColorAdministration remains in the hands of ourplant team, to
/// change CI for partners in correlation with the style \<br />\<br/>- if
/// you need new colors, contact the ourplant team for definitions\<br />
/// \</para>
/// \</summary>
```

Dabei handelt es sich um ein internes Package, von Häcker Automation
selbst entwickelt. Diese Implementierung dient der zentralen Definition
von Farben (Styling). Die Unit „opdarwin.graphics.Colors.pas" ist eine
extern eingebundene Unit, nicht Teil von OurPlantOS.

## Delphi-Modbus

> - Delphi ModbusTCP components
> - Downloads are available [here](https://github.com/coassoftwaresystems/delphi-modbus)
> - N.B. The old downloads are still available from SourceForge.net
> - A listing of implementations using the components is available in the wiki.

https://github.com/coassoftwaresystems/delphi-modbus

## Web.Win.Sockets

Das Package „Web.Win.Sockets" ist ein veraltetes, noch mit Delphi XE5
ausgeliefertes Package zur Netzwerkkommunikation. Die Versionen nach XE5
enthalten dieses Paket nicht mehr.

http://docwiki.embarcadero.com/RADStudio/Rio/en/Working_with_Sockets_Index

Alle Implementierungen mit diesen Klassen müssen mit anderen Packages,
zum Beispiel Indy, neu erstellt werden.

## SDAC (SQL Server Data Access Components)

Dies ist eine kommerzielle Abhängigkeit, eine Komponentensammlung des
Herstellers *Devart*. Sie dient dem Zugriff auf Datenbanken.

> SQL Server Data Access Components (SDAC) is a library of components
> that provides native connectivity to SQL Server from Delphi and
> C++Builder including Community Edition, as well as Lazarus (and Free
> Pascal) for Windows, Linux, macOS, iOS, and Android for both 32-bit
> and 64-bit platforms. SDAC-based applications connect to SQL Server
> directly through OLE DB, which is a native SQL Server interface. SDAC
> is designed to help programmers develop faster and cleaner SQL Server
> database applications.
>
> SDAC, a high-performance and feature-rich SQL Server connectivity
> solution, is a complete replacement for standard SQL Server
> connectivity solutions and presents an efficient native alternative to
> the Borland Database Engine (BDE) and standard dbExpress driver for
> access to SQL Server.

https://www.devart.com/sdac/?gclid=EAIaIQobChMI0-jWz-ml7AIVideyCh1yzQBUEAAYASAAEgJcOvD_BwE

## Delphi IDE Colorizer (VCL Styles Utils)

> The VCL Styles Utils is a Delphi library which extend the RAD Studio
> VCL Styles, adding unique features like the support for Classic and
> New Common dialogs, Task Dialogs, Styling of popup and shell menus,
> Non client area components and much more.

https://github.com/RRUZ/vcl-styles-utils

Oder die Elemente aus "Delphi IDE Colorizer":

> The Delphi IDE Colorizer (DIC) is a plugin which allows to skin the
> look and feel of the workspace of the RAD Studio IDE and Appmethod.
>
> **Features:**
> DIC is compatible with RAD Studio XE2-XE8, 10 Seattle, 10.1 Berlin,
> 10.2 Tokyo.
>
> Support for VCL Styles.
>
> Transparent menus.
>
> Allow to change the icons, set the colors, and gradient direction of
> the title bar of the docked windows.
>
> Set the colors, and gradient direction of the IDE toolbars.
>
> Improve the drawing of the disabled icons used in menus and toolbars
>
> Compatible with CnWizards and GExperts
>
> Replace the icons used in the gutter and the debugger.
>
> Includes 80+ themes.

https://github.com/RRUZ/Delphi-IDE-Colorizer

## EurekaLog

Dies ist eine kommerzielle Abhängigkeit.

> EurekaLog contains all the features that you need in a bug resolution
> system.
>
> It comes in 3 editions:
>
> Trial - it\'s fully functional edition with (one and only) additional
> limitation: any application, which is compiled with Trial edition of
> EurekaLog will expire after 30 days. There will be error message box
> after 30 days and application will exit. Trial itself may be used for
> infinite time. This edition can be used to evaluate EurekaLog. You
> cannot use this edition for any commercial development.
>
> Professional - it\'s fully function edition. This is minimum edition
> to do actual work except evaluating (including commercial
> development).
>
> Enterprise - it\'s the same as Professional edition, except it
> additionally offers full source code of EurekaLog.

<https://www.eurekalog.com/buy.php>



# Umstellung auf Packages

Siehe "[Umstellung auf
Packages.docx](https://haeckerautomation0.sharepoint.com/sites/Software/Freigegebene%20Dokumente/General/Emarcadero%20Entwicklungsumgebung/Umstellungen%20auf%20Packages.docx)"

# Sonstiges

Zu den Details der Quelltextumstellungen für DX10.3 existiert ein
weiteres Dokument, ein Auszug aus einem Ticketsystem (Redmine) mit dem
Namen „Tickets - OurPlant - Redmine.pdf".

Falls in der „Unit Hauptseite" ein Fehler auftritt, muss bisher als
Übergangslösung die Zeile 29 ({\$IF CompilerVersion \>=
28.0}System.ImageList,{\$ENDIF}) auskommentiert werden.

Wenn der Compiler beim Compilieren des OurPlant OS bestimmte .res
Dateien nicht finden kann, muss der Pfad

C:\\Development\\OurPlant\\ThirdParty\\jvcl\\jvcl\\resources

Und

C:\\Program Files
(x86)\\Embarcadero\\Studio\\20.0\\lib\\\$(Platform)\\\$(Config)

Als BibliotheksPfad hinzugefügt werden.
