# Installation<!-- omit in toc -->

Ausgehend von einer Windows 10 Installation mit allen aktuellen Patches,
sind die im nachfolgenden beschriebenen Installationsschritte auszuführen.

**Installationsschritte:**

- [Installation von Delphi 10.3 Rio](#installation-von-delphi-103-rio)
- [Installation von JCL](#installation-von-jcl)
- [Installation JVCL](#installation-jvcl)
- [Installation von Spring4D](#installation-von-spring4d)
  - [Projekte bauen](#projekte-bauen)
  - [Ausgabepfad zum Bibliothekspfad hinzufügen](#ausgabepfad-zum-bibliothekspfad-hinzufügen)
- [Installation von Delphi-Mocks](#installation-von-delphi-mocks)
  - [Suchpfad in den nutzenden Projekten anpassen](#suchpfad-in-den-nutzenden-projekten-anpassen)
- [Installation von Delphi-Modbus](#installation-von-delphi-modbus)
  - [Projekte bauen](#projekte-bauen-1)
  - [Suchpfad in den nutzenden Projekten anpassen](#suchpfad-in-den-nutzenden-projekten-anpassen-1)
- [Ersatz für Delphi IDE Colorizer](#ersatz-für-delphi-ide-colorizer)
- [Installation von OPDarwin](#installation-von-opdarwin)
- [Ersatz für Web.Win.Sockets](#ersatz-für-webwinsockets)
- [Installation von SDAC](#installation-von-sdac)
- [Cnpack](#cnpack)
  - [Installation per Setupsatz](#installation-per-setupsatz)
  - [Installation mit eigenem Build (optional)](#installation-mit-eigenem-build-optional)
- [MMX (CodeExplorer)](#mmx-codeexplorer)
- [Umgebungsvariable SVN](#umgebungsvariable-svn)


**Hinweis:** Die zu verwendenden Versionen der Drittabhängigkeiten werden unter `OurPlant\ThirdParty` bereitgestellt. Diesen Ordner bitte
auschecken und die darin enthaltenen Quellen zum Bauen der
Drittabhängigkeiten benutzen!

---

## Installation von Delphi 10.3 Rio

Die Entwicklungsumgebung Delphi 10.3 Rio und die zugehörigen Komponenten müssen installiert werden. Das sollte nach der [Anleitung von Embarcadero](https://www.embarcadero.com/resources/videos?aldSet=en-GB) erfolgen. Es müssen mindestens die Plattformen Win32 und Win64, sowie VCL installiert werden.

<img src="/supplementary/pictures/image4.png" width="350" title="RAD Studio Install Monitor">
<img src="/supplementary/pictures/image5.png" width="350" title="RAD Studio Install Monitor">

Die Samples, TeeChart, IBX und DUnit sollten mit installiert werden.

Erst wenn die Delphi IDE installiert ist, können die Drittabhängigkeiten installiert werden.

---

## Installation von JCL

Für die Installation wird GIT benötigt. Dafür einfach die Installationsdatei von [Git - Downloads (git-scm.com)](https://git-scm.com/downloads) herunterladen und ausführen.

Folgendes Menü in der IDE öffnen:

> Tools &rarr; GetIt-Package Manager 

anschließend nach JEDI suchen und das JCL Package installieren.

**Alternative:**

Zur Installation einfach in

> $(MyDevPath)\\OurPlant\\ThirdParty\\jcl\\jcl 

`install.bat`-Script ausführen und den Anweisungen folgen.

Möglicherweise versucht der Virenschutz die Erstellung der EXE-Dateien zu verhindern. In diesem Fall den $(MyDevPath) Ordner in die Whitelist des Virenschutzes aufnehmen.

Die Registrierung an der IDE erfolgt automatisch.

---

## Installation JVCL

Folgendes Menü in der IDE öffnen:
 
> Tools &rarr; GetIt-Package Manager 

anschließend nach JEDI suchen und das JVCL Package installieren.

**Alternative:**
Zur Installation einfach in

> $(MyDevPath)\\OurPlant\\ThirdParty\\jvcl\\jvcl 

das `install.bat`-Script ausführen und den Anweisungen folgen.

Die Registrierung an der IDE erfolgt automatisch.

---

## Installation von Spring4D

### Projekte bauen 

Unter 

> $(MyDevPath)\\OurPlant\\ThirdParty\\spring4d\\ 

die `Build.exe` ausführen und in der Oberfläche des Installers den Punkt `Delphi registry updaten` dazuwählen.

### Ausgabepfad zum Bibliothekspfad hinzufügen

Folgendes Menü in der IDE öffnen:

> Tools &rarr; Optionen &rarr; Sprache &rarr; Delphi &rarr; Bibliothek &rarr; Pfad der Bibliothek

Dem Bibliothekspfad muss der Ausgabeordner von Spring4D hinzugefügt werden.

> C:\\Development\\OurPlant\\ThirdParty\\spring4d\\Library\\Delphi10Rio\\\$(Platform)\\\$(Config)

Dieser Vorgang muss leider für alle Plattformen (Win32 und Win64) getrennt erledigt werden. 

---

##  Installation von Delphi-Mocks

Während der Analysephase zur Umstellung auf DX10.3 wurde die
Abhängigkeit vom Projekt OutPlantOS.exe restlos entfernt. Dennoch nutzen
andere Projekte, OurPlantOS.Test.exe, das Framework Delphi-Mocks und
daher bleibt diese Abhängigkeit generell zwar erhalten, wird aber in den
einzelnen Projekten selbst referenziert.

Zum Installieren muss unter

> C:\\Development\\OurPlant\\ThirdParty\\Delphi-Mocks\\packages

die DelphiMocksDX10Rio-Projektdatei geöffnet und erzeugt werden.

Es handelt sich um ein Mocking-Framework, welches in den Produktivprojekten nicht eingesetzt werden sollte (daher auch hier keine Abhängigkeit), wohl aber in den Testprojekten.

###  Suchpfad in den nutzenden Projekten anpassen

Folgendes Menü in der IDE öffnen:

> Tools &rarr; Optionen &rarr; Sprache &rarr; Delphi &rarr; Bibliothek

Dem Bibliothekspfad muss der Ausgabeordner von Delphi-Modbus hinzugefügt werden:

> $(MyDevPath)\\OurPlant\\ThirdParty\\Delphi-Mocks\\lib\\DX10Rio\\dcu\\\$(Platform)\\\$(Config)

---

##  Installation von Delphi-Modbus

###  Projekte bauen

Die Projektgruppe PackagesDelphiModbus103Rio.groupproj für DX10.3 Rio laden:

> $(MyDevPath)\\OurPlant\\ThirdParty\\delphi-modbus\\packages\\PackagesDelphiModbus103Rio.groupproj

Anschließend für alle Plattformen (Win32/Win64) und Konfigurationen (Debug/Release) erzeugen.

### Suchpfad in den nutzenden Projekten anpassen

Folgendes Menü in der IDE öffnen:

> Tools &rarr; Optionen &rarr; Sprache &rarr; Delphi &rarr; Bibliothek

Dem Bibliothekspfad muss der Ausgabeordner von Delphi-Modbus hinzugefügt
werden.

> $(MyDevPath)\\OurPlant\\ThirdParty\\delphi-modbus\\binaries\\\$(Platform)\\\$(Config)

---

## Ersatz für Delphi IDE Colorizer

Die Umbaumaßnahmen der Quellen hin zu einer mit Delphi DX10.3 erstellbaren Version führte zum Entfernen dieser Abhängigkeit.

Zwar kann das Projekt gerne aus dem entsprechenden Repositorium geklont werden, eine Notwendigkeit dafür besteht nicht.

---

##  Installation von OPDarwin

Dieses Paket ist kein externes Paket, es wurde von Häcker Automation
selbst erstellt. Es muss von einer internen Quelle in den lokalen Ordner
für Drittabhängigkeiten kopiert werden. Zum aktuellen Zeitpunkt ist
unklar, ob es dafür in Zukunft ein eigenes Repo geben wird.

Der Suchpfad der nutzenden Anwendungen wurde bereits auf einen Pfad
relativ zum aktuellen Repositorium gesetzt, werden die OPDarwin Quellen
entsprechend kopiert, besteht neben dem Bauen des Packages kein weiterer
Handlungsbedarf.

Der Suchpfad

> $(MyDevPath)\\OurPlant\\OPDarwin\\opdarwin\\graphics

muss noch unter 

> Projekt &rarr; Optionen &rarr; DelphiCompiler 

für alle Platformen (Win32/Win64) eingetragen werden.

**Kommentar:** Eine Integration über den Bibliothekspfad, etwa wie bei Spring4D, wurde hier absichtlich nicht umgesetzt, da es sich nur um eine einzelne, einfache Datei handelt. Dieser Teil hat hohes Potential, als Abhängigkeit in später erstellte Packages zu gelangen, oder ganz zu verschwinden.

---

## Ersatz für Web.Win.Sockets

Die Umbauten für eine OurPlantOS-Version für den DX10.3-Compiler führten
zur Entfernung dieser Abhängigkeit. Das Package wurde bis zu Delphi XE5
von Embarcadero mitgeliefert, danach als veraltet definiert und nicht
weiter gewartet.

In Zukunft sollten die Indy-Komponenten, welche schon immer wichtiger
Teil von Delphi selbst sind, für die Netzwerkkommunikation verwendet
werden. Die von Web.Win.Sockets abhängige Datei „system.ServerLog.pas"
wurde bereits auf Indy umgestellt.

---

## Installation von SDAC

Die externe und kommerzielle Abhängigkeit SDAC muss mit dem vom Hersteller zur Verfügung gestellten Installationssatz nach Herstelleranleitung installiert werden. Dabei sollten alle IDE\'s geschlossen sein.

---

## Cnpack

### Installation per Setupsatz

Ein Setupsatz kann einfach heruntergeladen werden:

> <http://www.cnpack.org/showlist.php?id=39&lang=en>

Bei der Installation sollten alle IDE\'s geschlossen sein.

### Installation mit eigenem Build (optional)

Als Open-Source-Projekt kann CnPack auch einfach aus dem Repository heraus erstellt und genutzt werden.

Clone von:

> <https://github.com/cnpack/cnwizards.git>

Packages Erstellen, dann Packages installieren und die IDE-Erweiterungen stehen zur Verfügung. Bei der Installation sollten alle IDE\'s geschlossen sein.

---

## MMX (CodeExplorer)

Der MMX CodeExplorer ist Freeware und ohne Einschränkungen auch kommerziell nutzbar. Dieser ist jedoch nicht Open-Source und daher bleibt nur die Installation per Setupsatz:

> <https://www.mmx-delphi.de/>

---

## Umgebungsvariable SVN

Die SVN-Informationen werden benötigt um automatisiert eine Revisonsnummer in der Oberfläche anzeigen zu können.

**HINWEIS:** Diese Abhängigkeit wird demnächst vereinheitlicht.

In den Systemumgebungsvariablen ist SVNEXE=C:\\Program
Files\\Tortiose\\bin\\svn.exe nachzutragen, da sonst folgende
Fehlermeldung beim erzeugen auftritt:

![](/supplementary/pictures/image12.jpg)

Die Umgebungsvariablen findet man im Win10 am besten über die
Start-Suche.

![](/supplementary/pictures/image13.jpg)