Within this repository you will find all necessary documents for working on OurPlantOS.

### Content
- [installation manual](https://gitlab.com/ourplant.net/ourplant_os/documents/development-environment/-/blob/main/required%20tools/manual.md)
- [required tools](https://gitlab.com/ourplant.net/ourplant_os/documents/development-environment/-/blob/main/required%20tools/required%20tools.md)
- [design rules](https://gitlab.com/ourplant.net/ourplant_os/documents/development-environment/-/blob/main/design%20rules/design%20rules.md)
- [release management](https://gitlab.com/ourplant.net/ourplant_os/documents/development-environment/-/blob/main/release%20management/release%20management.md)

<!-- ---------------------------------------
Copyright &copy; 2021 Häcker Automation  -->