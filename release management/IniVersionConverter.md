# Konverter für Änderungen in den Ini-Dateien
online version bei [codi](https://md.kabi.tk/pjm5XVULTUuzn0sA4hK3vQ?view)

## Problemstellung
Durch Änderungen in der Software ergeben sich Änderungen in der Art und Weise, wie die Einstellungen in der Ini gespeichert werden, wie z.B.:
* Umbenennen von Bezeichnern
* aus einem festen Array wird eine flexible Liste
* Umstrukturierung in der Software
  * Unterkomponente wird nicht mehr fest eingebaut, sondern über Link zugeordnet
* Verschieben von Einstellungen in andere Objekte (Verlagerung von Zuständigkeiten)
  * Einstellungen zum Magazin, die vorher im Bauteil gespeichert wurden, wandern ins Magazin
* Neuentwicklung von bereits bestehenden Komponenten
  * JTF hat alte, wie auch neue Implementierung

Um die Kompatibilität bei Softwareupdates auf Kundenmaschinen aufrechtzuerhalten (und damit auch ein gewisses Maß an Sicherheit), brauchen wir eine Lösung

## Lösungsansatz
Vielleicht gibt es ja schon ein offenes Projekt, dass sich mit dem Thema befasst.

Effektiv müssen die Inis von einer Version zur nächsten konvertiert werden. Die Informationen was und wie die Inis geändert werden müssen, können nur von den Entwicklern kommen.
Wir brauchen also etwas, was alte Inis in neueren Versionen lesbar macht.
Möglichkeiten:
- in **OPOS** einen Konverter einbauen
- ein **separates Delphi-Projekt**
- eigener **Scriptinterpreter** (als Vorbild jener von Hilde)
- **Python** Script
- Konvertertool (belibiger Sprache) mit JSON Konverterdatenbank(-Config) 

zu beachtende Problemstellungen:
- schreiben der Konverter muss vom Entwickler per Hand geschrieben werden 
  - man kann leicht was vergessen (Einträge, die konvertiert werden müssen)
  - sehr Fehleranfällig &rarr; muss getestet werden (das Lesen der neuen Ini durch OPOS)
- der Konverter braucht die Definition von Typnummern (Komponenten, Positionen)
  - könnte als pre-build-Script über (python-)script aus der MPKomponenten.pas geparst werden

|                                 | pro                                                                         | con                                                                 | Entscheidung |
|---------------------------------|-----------------------------------------------------------------------------|---------------------------------------------------------------------|---------------|
| **in OPOS**                     | Die Pflege geschieht da wo sie auch verwendet wird                          | Build-Prozess dauert noch länger                                    | nicht bevorzugt wenn es hardcoded ist|
|                                 |                                                                             | bei Fehler im Converter muss ganzes Projekt neu gebaut werden       |
| **Delphi Projekt**              | leichter änderbar                                                           | muss bei Änderungen neu gebaut werden                               | nicht bevorzugt wenn es hardcoded ist|
|                                 | leichte Einbindung der Typnummern                                           | keine einfache Anpassung vor Ort möglich                            | **erste Wahl**
|                                 | gewohnte IDE                                                                |                                                                     | mit JSON config Dateien
|                                 | Compiler prüft Syntax (muss allerdings eh getestet werden)                  |                                                                     |
| **eigener Scriptinterpreter**   | Konfiguration über config-Dateien möglich (z.B. JSON)                       | umständlich erweiterbar um neue Funktionalität                      |
|                                 | leichte Erweiterung neuer Einträge                                          | kein Syntaxcheck von Konfigurationen (muss eh getestet werden)      |
|                                 | Logging ließe sich sicher mit einbauen (kann nicht vergessen werden)        | Spagetticode                                                        |
|                                 | Backup wäre gesichert                                                       |                                                                     |
|                                 | Funktionalität von unterstützten Aktionen wäre immer gleich und verifiziert |                                                                     |
|                                 | Applikation könnte auch mitarbeiten                                         |                                                                     |
| **Python Script**               | nachträglich änderbar (auch vor Ort)                                        | Typnummern müssen irgendwie importiert werden                       |
|                                 | wir müssen keine eigene Skriptsprache entwickeln                            | Python muss auf den Maschinen sein (Produktivsystem und Sicherheit) |
|                                 | exe bauen ist auch möglich                                                  | 
| **Objekt Konverter in Delphi**  | Arbeiten an origial Objekten                                                |  Alte Objekte archivieren   |nicht umsetzbar
|||uses Abhängigkeiten können nicht sichergestellt werden, ohne das ganze Hauptprojekt zu übernehmen|

## Lösungsvoschlag
- in Ini wird die svn-Revisionsnummer geschrieben 
- Sprung zu git und Hash beachten !
- Ourplant erkennt eigene Version (subversion)
  - weicht diese von der in der Ini ab, muss der Konverter aufgerufen werden
- Konverter ist Python-Projekt
  - wir bauen uns ein kleines Framework
  - zu jeder Revision (mit Ini-Änderungen) gibt es eine "Unit" (wäre schön)
  - überschreibt die aktuelle Ini 
    - .bak anlegen (irgendeine Möglichkeit zum Rückgängig machen)
    - Log schreiben
  - alle Inis müssen dem Konverter zur Verfügung gestellt werden (falls eine Konfiguration von der VicoBase in die Programmdatei wandert)
  - ~~Konvertierung Richtung **höhere** und auch **niedrigere** Version (für zurückspielen von Backups)~~
### JSON Konverter Config
Ein Tool in Python oder Delphi geschrieben konvertiert mit Hilfe einer JSON Datei über die Versionssprünge
{"Variable" : {"Versionssprung" : 1999, "Vorher" ... "Nachher" }}


## offene Punkte
1. wie können wir die Versionen der Releases vergleichen (bei Nightlies geht es per SVN Revisionsnummer) - wir müssen ja feststellen, welche Revisionen in der Release gelandet sind
2. Wie kann der Object Converter funktionieren?
3. Wie muss der Release-Prozess dafür erweitert werden?
 
## Kommentare aus der Besprechung 26.04.2022
- alte Einträge aus den Inis generell löschen?
  - Applikation wird streiken - vll auch nicht?
  - ist abgesetztes Thema - sollte vll in einem extra Termin geklärt werden
- mit JSON arbeiten anstelle von INIs
    - Vorteile: - ini speichert sowieso Objekte/Klassenvariablen
                - einfacher zu parsen + sicherung über Typisierung 
                - einfache sicherung einer "sauberen" config 
                - Hierarchien besser darstellbar
                - Lesbarkeit? 
                - unterstützung von datentypen
                - Muster können vom Quellcode automatisch erstellt werden 
                - Erstellung von "TreeView"-mäßiger Nutzeroberfläche für applikation
                
    - Nachteile: - hin und her konvertieren zwischen versionssprüngen könnte probleme machen      
                 - keine "teilweise" konvertierung möglich     
                 - vlt gegendruck von applikation wegen neuem system? 
                 

# Ergebnis:
- Delphi projekt mit JSON konfiguration
- später vll Editor
- vll noch Aufräumoptionen (alle nicht genutzten Einträge entfernen)




